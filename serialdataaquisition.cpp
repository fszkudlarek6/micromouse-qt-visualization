#include "serialdataaquisition.h"

void serialDataAquisition::initSerialPort()
{
    serialPort = new QSerialPort();
    serialPort->setPortName(SERIAL_PORT_NAME);
    serialPort->setBaudRate(QSerialPort::BaudRate::Baud115200);
    serialPort->setParity(QSerialPort::Parity::NoParity);
    serialPort->setDataBits(QSerialPort::DataBits::Data8);
    serialPort->setStopBits(QSerialPort::StopBits::OneStop);
    serialPort->setFlowControl(QSerialPort::FlowControl::NoFlowControl);
    serialPort->open(QIODevice::ReadOnly);

    if(serialPort->isOpen()){
        qDebug() <<"Serial is open.";
        qDebug() << serialPort->error();
    }

    connect(serialPort, SIGNAL(readyRead()), this, SLOT(readData()));
}

void serialDataAquisition::dataToVect(QList<QList<double> > &dataVect, std::stringstream &dataSS, int mode)
{
    std::string num[3];
    for (int i=0; i < 3; ++i) {
        dataSS >>num[i];
    }
    std::string originalMessage;
    switch (mode) {
    case 0:
        originalMessage = "ACC: ";
        break;
    case 1:
        originalMessage = "GYR: ";
        break;
    case 2:
        originalMessage = "MAG: ";
        break;
    }
    originalMessage +=num[0]+" "+num[1]+" "+num[2]+ " ";
    uint32_t calculated_crc = calc_crc(originalMessage);
    std::string CRC;
    dataSS >> CRC;
    if(CRC == "CRC:"){
        dataSS >> CRC;
        uint32_t received_crc = uint32_t(std::stoll(CRC));
        if(received_crc == calculated_crc){
            for (int i=0; i < 3; ++i) {
                //for simulated data:
                //dataVect[i].push_back(double(std::stoi(num[i])));
                //for data from IMU:
                dataVect[i].push_back( (double(std::stoi(num[i])))/100 );
            }
        }
        else{
            qDebug() <<"Error: received and calculated CRC does not match\n";
            exit(1);
        }
    }
    else{
        qDebug() <<"Error while reading CRC\n";
        exit(1);
    }
}

void serialDataAquisition::dataToBoard(std::stringstream &dataSS)
{
    //TODO: add operations of adding data to board
    std::string originalMessage;
    originalMessage = "LAB: ";

    std::string codedLabData;
    dataSS >> codedLabData;

    originalMessage += codedLabData + " ";

    uint32_t calculated_crc = calc_crc(originalMessage);
    std::string CRC;
    dataSS >> CRC;
    if(CRC == "CRC:"){
        dataSS >> CRC;
        uint32_t received_crc = uint32_t(std::stoll(CRC));
        if(received_crc == calculated_crc){
            labBoard->updateLabirinth(codedLabData);
        }
        else{
            qDebug() <<"Error: received and calculated CRC does not match\n";
            exit(1);
        }
    }
    else{
        qDebug() <<"Error while reading CRC\n";
        exit(1);
    }
}

void serialDataAquisition::dataToFloodFill(std::stringstream &dataSS)
{
    //TODO: add operations of adding data to board
    std::string originalMessage;
    originalMessage = "FLOOD: ";

    std::string codedFloodData;
    dataSS >> codedFloodData;

    originalMessage += codedFloodData + " ";

    uint32_t calculated_crc = calc_crc(originalMessage);
    std::string CRC;
    dataSS >> CRC;
    if(CRC == "CRC:"){
        dataSS >> CRC;
        uint32_t received_crc = uint32_t(std::stoll(CRC));
        if(received_crc == calculated_crc){
            labBoard->updateFloodFill(codedFloodData);
        }
        else{
            qDebug() <<"Error: received and calculated CRC does not match\n";
            exit(1);
        }
    }
    else{
        qDebug() <<"Error while reading CRC\n";
        exit(1);
    }
}


void serialDataAquisition::dataToRobot(std::stringstream &dataSS)
{
    std::string num[3];
    for (int i=0; i < 3; ++i) {
        dataSS >>num[i];
    }
    std::string originalMessage;
    originalMessage = "ROB: ";

    originalMessage +=num[0]+" "+num[1]+" "+num[2]+ " ";
    uint32_t calculated_crc = calc_crc(originalMessage);
    std::string CRC;
    dataSS >> CRC;
    if(CRC == "CRC:"){
        dataSS >> CRC;
        uint32_t received_crc = uint32_t(std::stoll(CRC));
        if(received_crc == calculated_crc){
            labBoard->updateRobot(std::stoi(num[0]), std::stoi(num[1])
                                  , std::stoi(num[2]));
        }
        else{
            qDebug() <<"Error: received and calculated CRC does not match\n";
            exit(1);
        }
    }
    else{
        qDebug() <<"Error while reading CRC\n";
        exit(1);
    }
}

uint32_t serialDataAquisition::calc_crc(std::string &dataS)
{
    uint32_t crc_table[256];
    uint32_t crc = 0xFFFFFFFF; // Initial CRC value
    const char * data = dataS.c_str();
    // Generate CRC32 lookup table
    for (int i = 0; i < 256; i++) {
        uint32_t crc_entry = i;
        for (int j = 0; j < 8; j++) {
            crc_entry = (crc_entry & 1) ? (crc_entry >> 1) ^ 0xEDB88320 : crc_entry >> 1;
        }
        crc_table[i] = crc_entry;
    }

    // Calculate CRC32
    while (*data != '\0') {
        crc = (crc >> 8) ^ crc_table[(crc ^ *data) & 0xFF];
        data++;
    }

    return ~crc; // Final CRC value (bitwise complement)
}

void serialDataAquisition::readData()
{
    if(serialPort->isOpen()){
        while(serialPort->bytesAvailable()){
            dataFromSerial += serialPort->readAll();
            if(dataFromSerial.at(dataFromSerial.length() - 1) == char(10)){
                isDataReceived = true;
            }
        }
        if(isDataReceived == true){
            //qDebug() << "Data from serial :" << dataFromSerial;
            std::string conv = dataFromSerial.toStdString();
            std::stringstream ss(conv);
            std::string word;
            while(ss >> word){
                //QString temp = QString::fromStdString(word);
                if(word == "ACC:"){
                    this->dataToVect(accData, ss, 0);
                }
                else if(word == "GYR:"){
                    this->dataToVect(gyrData, ss, 1);
                }
                else if(word == "MAG:"){
                    this->dataToVect(magData, ss, 2);
                }
                else if(word == "LAB:"){
                    this->dataToBoard(ss);
                }
                else if(word == "ROB:"){
                    this->dataToRobot(ss);
                }
                else if(word == "FLOOD:"){
                    this->dataToFloodFill(ss);
                }
            }
            dataFromSerial = "";
            isDataReceived = false;
        }
    }
}

serialDataAquisition::serialDataAquisition(board *board)
{
    accData.resize(3);
    gyrData.resize(3);
    magData.resize(3);

    labBoard = board;

    initSerialPort();
}

serialDataAquisition::~serialDataAquisition()
{

}



