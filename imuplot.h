#ifndef IMUPLOT_H
#define IMUPLOT_H

#include <QObject>
#include <QDebug>
#include "qcustomplot.h"

/**
 * @class imuPlot
 * @brief The imuPlot class manages plotting IMU data using QCustomPlot.
 *
 * The imuPlot class manages plotting IMU data using QCustomPlot.
 */
class imuPlot : public QObject
{
    Q_OBJECT

    /// Pointer to the list of data points for plotting.
    QList<QList<double>> * dataPointer;

    /// Pointer to the graph for the X-axis data.
    QPointer<QCPGraph> mGraphX;

    /// Pointer to the graph for the Y-axis data.
    QPointer<QCPGraph> mGraphY;

    /// Pointer to the graph for the Z-axis data.
    QPointer<QCPGraph> mGraphZ;

public:
    /**
     * @brief Constructs an imuPlot object.
     *
     * Constructs an imuPlot object.
     * @param[in] inputDataPointer - Pointer to the list of input data points.
     */
    explicit imuPlot(QList<QList<double>> * inputDataPointer);

    /// Pointer to the QCustomPlot object used for plotting.
    QCustomPlot *mPlot;

    /**
     * @brief Changes the data points used for plotting.
     *
     * Changes the data points used for plotting.
     * @param[in] inputDataPointer - Pointer to the new list of input data points.
     */
    void changePlotData(QList<QList<double>> * inputDataPointer);

    /**
     * @brief Function to retranslate series names and time axis name.
     *
     * Function to retranslate series names and time axis name.
     */
    void retranslateSeriesNames();

    /// Timer for updating the plot data.
    QTimer mDataTimer;

private slots:
    /**
     * @brief Slot function for handling timer events to update the plot.
     *
     * Slot function for handling timer events to update the plot.
     */
    void timerSlot();

signals:
};

#endif // IMUPLOT_H
