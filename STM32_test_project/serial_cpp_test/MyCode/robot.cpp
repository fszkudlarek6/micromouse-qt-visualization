/*
 * robot.cpp
 *
 *  Created on: May 18, 2024
 *      Author: phelipe
 */

#include <string>

#include <robot.h>

robot::robot(board *currentBoard) {
	position_x = 0;
	position_y = 0;
	position_angle = 90;

	labBoard = currentBoard;

	labBoard->markVisited(position_x, position_y);
	labBoard->addLeftWall(position_x, position_y);

	movingUp = true;
	turning = false;
	turnCompleted = false;
	col = 0;
}

robot::~robot() {
	// TODO Auto-generated destructor stub
}

void robot::get_rob_data(char ret[][50]) {
	std::string ret_string;
	ret_string = "ROB: " + std::to_string(position_x) + " "
			+ std::to_string(position_y) + " " + std::to_string(position_angle)
			+ " ";
	std::strcpy(*ret, ret_string.c_str());
}

void robot::run_forest() {
	if (position_angle == 90) {
		position_y++;
		if (position_y == LAB_SIZE) {
			position_angle = 0;
			position_y--;
		} else {
			if (position_y != LAB_SIZE - 1) {
				labBoard->addLeftWall(position_x, position_y);
				labBoard->addRightWall(position_x, position_y);
			}
		}
	} else if (position_angle == 0) {
		position_x++;
		if (position_x == LAB_SIZE) {
			position_angle = 270;
			position_x--;
		} else {
			if (position_x != LAB_SIZE - 1) {
				labBoard->addTopWall(position_x, position_y);
				labBoard->addBotWall(position_x, position_y);
			}
		}
	} else if (position_angle == 270) {
		position_y--;
		if (position_y == 255) {
			position_angle = 180;
			position_y++;
		} else {
			if (position_y != 0) {
				labBoard->addLeftWall(position_x, position_y);
				labBoard->addRightWall(position_x, position_y);
			}
		}
	} else if (position_angle == 180) {
		position_x--;
		if (position_x == 255) {
			position_angle = 90;
			position_x++;
		} else {
			if (position_x != 0) {
				labBoard->addTopWall(position_x, position_y);
				labBoard->addBotWall(position_x, position_y);
			}
		}
	}

	labBoard->markVisited(position_x, position_y);
}

void robot::exploreColumnsStep() {
	if (turning) {
		if (!turnCompleted) {
			// Handle turning at the top or bottom of a column
			if (movingUp) {
				// Turn right at the top of a column
				if (position_angle == 90) {
					position_angle = 0;
					turnCompleted = true;
				}
			} else {
				// Turn right at the bottom of a column
				if (position_angle == 270) {
					position_angle = 0;
					turnCompleted = true;
				}
			}
		} else {
			// Move one cell right after completing the turn
			if (position_x < LAB_SIZE - 1) {
				position_x++;
				labBoard->markVisited(position_x, position_y);

				// Add walls for the new position
				if (movingUp) {
					labBoard->addTopWall(position_x, position_y);
				} else {
					labBoard->addBotWall(position_x, position_y);
				}

				// Prepare for the next column exploration
				turning = false;
				turnCompleted = false;
				movingUp = !movingUp; // Change direction for the next column
			}
		}
	} else {
		if (movingUp) {
			// Moving up the current column
			if (position_y < LAB_SIZE - 1) {
				position_y++;
				position_angle = 90;
				labBoard->markVisited(position_x, position_y);

				// Add walls for the current position
				if (position_y < LAB_SIZE - 1) {
					labBoard->addLeftWall(position_x, position_y);
					labBoard->addRightWall(position_x, position_y);
				}
			} else {
				// Reached the top, prepare to turn right
				turning = true;
			}
		} else {
			// Moving down the current column
			if (position_y > 0) {
				position_y--;
				position_angle = 270;
				labBoard->markVisited(position_x, position_y);

				// Add walls for the current position
				if (position_y > 0) {
					labBoard->addLeftWall(position_x, position_y);
					labBoard->addRightWall(position_x, position_y);
				}
			} else {
				// Reached the bottom, prepare to turn right
				turning = true;
			}
		}
	}
}

bool robot::moveToPosition(uint8_t x, uint8_t y){
	if(isValidPosiotion(x, y)){
		position_x = x;
		position_y = y;
		labBoard->markVisited(x, y);
		labBoard->floodCell(x, y);
		return true;
	}
	else{
		return false;
	}
}

void robot::turn(int turn_angle){
	position_angle += turn_angle;
	position_angle %= 360;
	if (position_angle < 0){
		position_angle = 360 + position_angle;
	}
}

bool robot::isValidPosiotion(uint8_t x, uint8_t y){
	if(((0 <= x) & (x < LAB_SIZE)) && ((0 <= y) & (y < LAB_SIZE))){
		return true;
	}
	else {
		return false;
	}
}

bool robot::canGoFront(uint8_t *cell_to_move_position_x, uint8_t *cell_to_move_position_y){
	switch (position_angle)
	{
	case 0:
		if(labBoard->hasRightWall(position_x, position_y)){
			return false;
		}
		else{
			*cell_to_move_position_x = position_x + 1;
			*cell_to_move_position_y = position_y;
			return true;
		}
		break;
	case 90:
		if(labBoard->hasTopWall(position_x, position_y)){
			return false;
		}
		else{
			*cell_to_move_position_x = position_x;
			*cell_to_move_position_y = position_y + 1;
			return true;
		}
		break;
	case 180:
		if(labBoard->hasLeftWall(position_x, position_y)){
			return false;
		}
		else{
			*cell_to_move_position_x = position_x - 1;
			*cell_to_move_position_y = position_y;
			return true;
		}
		break;
	case 270:
		if(labBoard->hasBotWall(position_x, position_y)){
			return false;
		}
		else{
			*cell_to_move_position_x = position_x;
			*cell_to_move_position_y = position_y - 1;
			return true;
		}
		break;
	default:
		return false;
		break;
	}
}

bool robot::canGoRight(uint8_t *cell_to_move_position_x,
				uint8_t *cell_to_move_position_y){
		switch (position_angle)
	{
	case 0:
		if(labBoard->hasBotWall(position_x, position_y)){
			return false;
		}
		else{
			*cell_to_move_position_x = position_x;
			*cell_to_move_position_y = position_y - 1;
			return true;
		}
		break;
	case 90:
		if(labBoard->hasRightWall(position_x, position_y)){
			return false;
		}
		else{
			*cell_to_move_position_x = position_x + 1;
			*cell_to_move_position_y = position_y;
			return true;
		}
		break;
	case 180:
		if(labBoard->hasTopWall(position_x, position_y)){
			return false;
		}
		else{
			*cell_to_move_position_x = position_x;
			*cell_to_move_position_y = position_y + 1;
			return true;
		}
		break;
	case 270:
		if(labBoard->hasLeftWall(position_x, position_y)){
			return false;
		}
		else{
			*cell_to_move_position_x = position_x - 1;
			*cell_to_move_position_y = position_y;
			return true;
		}
		break;
	default:
		return false;
		break;
	}
}

bool robot::canGoLeft(uint8_t *cell_to_move_position_x,
				uint8_t *cell_to_move_position_y) {
	switch (position_angle)
	{
	case 0:
		if(labBoard->hasTopWall(position_x, position_y)){
			return false;
		}
		else{
			*cell_to_move_position_x = position_x;
			*cell_to_move_position_y = position_y + 1;
			return true;
		}
		break;
	case 90:
		if(labBoard->hasLeftWall(position_x, position_y)){
			return false;
		}
		else{
			*cell_to_move_position_x = position_x - 1;
			*cell_to_move_position_y = position_y;
			return true;
		}
		break;
	case 180:
		if(labBoard->hasBotWall(position_x, position_y)){
			return false;
		}
		else{
			*cell_to_move_position_x = position_x;
			*cell_to_move_position_y = position_y - 1;
			return true;
		}
		break;
	case 270:
		if(labBoard->hasRightWall(position_x, position_y)){
			return false;
		}
		else{
			*cell_to_move_position_x = position_x + 1;
			*cell_to_move_position_y = position_y;
			return true;
		}
		break;
	default:
		return false;
		break;
	}
}

bool robot::canGoBack(uint8_t *cell_to_move_position_x, uint8_t *cell_to_move_position_y){
	switch (position_angle)
	{
	case 0:
		if(labBoard->hasLeftWall(position_x, position_y)){
			return false;
		}
		else{
			*cell_to_move_position_x = position_x - 1;
			*cell_to_move_position_y = position_y;
			return true;
		}
		break;
	case 90:
		if(labBoard->hasBotWall(position_x, position_y)){
			return false;
		}
		else{
			*cell_to_move_position_x = position_x;
			*cell_to_move_position_y = position_y - 1;
			return true;
		}
		break;
	case 180:
		if(labBoard->hasRightWall(position_x, position_y)){
			return false;
		}
		else{
			*cell_to_move_position_x = position_x + 1;
			*cell_to_move_position_y = position_y;
			return true;
		}
		break;
	case 270:
		if(labBoard->hasTopWall(position_x, position_y)){
			return false;
		}
		else{
			*cell_to_move_position_x = position_x;
			*cell_to_move_position_y = position_y + 1;
			return true;
		}
		break;
	default:
		return false;
		break;
	}
}

void robot::exploreLabirynthFront(){
	uint8_t next_x, next_y;
	if(canGoFront(&next_x, &next_y)){
		moveToPosition(next_x, next_y);
	}
	//cannot go front
	else{		
		if(canGoRight(&next_x, &next_y)){
			turn(-90);
			moveToPosition(next_x, next_y);
		}
		//cannot go front or right
		else{
			if(canGoLeft(&next_x, &next_y)){
				turn(90);
				moveToPosition(next_x, next_y);
			}
			//cannot go front, right, or left - in a deadend - has to go back
			else{
				//it simply has to be able to go some way - if not, robot is in a 1x1 square
				if(canGoBack(&next_x, &next_y)){
					turn(180);
					moveToPosition(next_x, next_y);
				}
			}	
		}
	}
}

void robot::exploreLabirynthRight(){
	uint8_t next_x, next_y;
	if(canGoRight(&next_x, &next_y)){
		turn(-90);
		moveToPosition(next_x, next_y);
	}
	//cannot go right
	else{		
		if(canGoFront(&next_x, &next_y)){
			moveToPosition(next_x, next_y);
		}
		//cannot go front or right
		else{
			if(canGoLeft(&next_x, &next_y)){
				turn(90);
				moveToPosition(next_x, next_y);
			}
			//cannot go front, right, or left - in a deadend - has to go back
			else{
				//it simply has to be able to go some way - if not, robot is in a 1x1 square
				if(canGoBack(&next_x, &next_y)){
					turn(180);
					moveToPosition(next_x, next_y);
				}
			}	
		}
	}
}

void robot::exploreLabirynthLeft(){
	uint8_t next_x, next_y;
	if(canGoLeft(&next_x, &next_y)){
		turn(90);
		moveToPosition(next_x, next_y);
	}
	//cannot go right
	else{
		if(canGoFront(&next_x, &next_y)){
			moveToPosition(next_x, next_y);
		}
		//cannot go front or right
		else{
			if(canGoRight(&next_x, &next_y)){
				turn(-90);
				moveToPosition(next_x, next_y);
			}
			//cannot go front, right, or left - in a deadend - has to go back
			else{
				//it simply has to be able to go some way - if not, robot is in a 1x1 square
				if(canGoBack(&next_x, &next_y)){
					turn(180);
					moveToPosition(next_x, next_y);
				}
			}
		}
	}
}

void robot::exploreLabirynthFrontModified(){
	uint8_t next_x, next_y;
	//this would work due to short circuit evaluation -- canGoFront will be executed first
	if(canGoFront(&next_x, &next_y) && (!labBoard->wasVisited(next_x, next_y))){
		moveToPosition(next_x, next_y);
	}
	//cannot go front or front already explored
	else{
		if((canGoRight(&next_x, &next_y)) && (!labBoard->wasVisited(next_x, next_y))){
			turn(-90);
			moveToPosition(next_x, next_y);
		}
		//cannot go front or right, or both are explored
		else{
			if((canGoLeft(&next_x, &next_y)) && (!labBoard->wasVisited(next_x, next_y))){
				turn(90);
				moveToPosition(next_x, next_y);
			}
			//cannot go front, right, or left, or all three are explored
			else{
				//just go anywhere to escape loop
				exploreLabirynthFront();
			}
		}
	}
}

void robot::exploreLabirynthRightModified(){
	uint8_t next_x, next_y;
	//this would work due to short circuit evaluation -- canGoFront will be executed first
	if((canGoRight(&next_x, &next_y)) && (!labBoard->wasVisited(next_x, next_y))){
		turn(-90);
		moveToPosition(next_x, next_y);
	}
	//cannot go front or front already explored
	else{
		if(canGoFront(&next_x, &next_y) && (!labBoard->wasVisited(next_x, next_y))){
			moveToPosition(next_x, next_y);
		}
		//cannot go front or right, or both are explored
		else{
			if((canGoLeft(&next_x, &next_y)) && (!labBoard->wasVisited(next_x, next_y))){
				turn(90);
				moveToPosition(next_x, next_y);
			}
			//cannot go front, right, or left, or all three are explored
			else{
				//just go anywhere to escape loop
				exploreLabirynthRight();
			}
		}
	}
}

void robot::exploreLabirynthLeftModified(){
	uint8_t next_x, next_y;
	//this would work due to short circuit evaluation -- canGoFront will be executed first
	if((canGoLeft(&next_x, &next_y)) && (!labBoard->wasVisited(next_x, next_y))){
		turn(90);
		moveToPosition(next_x, next_y);
	}
	//cannot go front or front already explored
	else{
		if(canGoFront(&next_x, &next_y) && (!labBoard->wasVisited(next_x, next_y))){
			moveToPosition(next_x, next_y);
		}
		//cannot go front or right, or both are explored
		else{
			if((canGoRight(&next_x, &next_y)) && (!labBoard->wasVisited(next_x, next_y))){
				turn(-90);
				moveToPosition(next_x, next_y);
			}
			//cannot go front, right, or left, or all three are explored
			else{
				//just go anywhere to escape loop
				exploreLabirynthLeft();
			}
		}
	}
}

void robot::goToLeastFloodFillWeight(){
	if(labBoard->getFloodFillWeight(position_x, position_y) != 0){
		int next_x = position_x;
		int next_y = position_y;
		int angle_after_position_change = position_angle;

		labBoard->getNeighbourWithLowestWeight(&next_x, &next_y, &angle_after_position_change);

		moveToPosition(next_x, next_y);
		position_angle = angle_after_position_change;
	}
}
