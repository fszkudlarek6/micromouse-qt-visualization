/*
 * maze_collection.h
 *
 *  Created on: Nov 28, 2024
 *      Author: filip
 */

#ifndef MAZE_COLLECTION_H_
#define MAZE_COLLECTION_H_

#include <string>

std::string apec2024 = R"(o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o
|                                                               |
o   o---o---o---o---o---o---o---o---o---o---o---o---o---o---o   o
|   |               |                                           |
o   o   o---o---o---o   o---o---o---o---o---o---o---o---o---o   o
|   |   |           |       |       |       |               |   |
o   o   o   o---o   o---o   o   o   o   o   o   o---o---o   o   o
|   |   |   |           |       |   |   |   |       |       |   |
o   o   o   o---o---o   o---o---o   o   o   o---o   o---o   o   o
|   |   |   |       |           |   |   |           |       |   |
o   o   o   o   o   o---o---o   o   o   o   o---o---o   o---o   o
|   |   |   |   |               |       |   |   |       |       |
o   o   o   o   o---o---o---o---o---o---o---o   o   o---o   o   o
|   |   |   |   |       |               |               |   |   |
o   o   o   o   o   o   o   o   o---o---o---o   o---o   o   o   o
|   |   |   |   |   |   |   | G   G |   |           |   |   |   |
o   o   o   o   o   o   o   o   o   o   o   o---o---o---o   o   o
|   |   |   |   |   |       | G   G |           |           |   |
o   o   o   o   o   o---o   o---o---o   o---o   o   o---o---o   o
|   |   |   |   |       |   |               |   |   |       |   |
o   o   o   o   o   o   o---o---o   o   o---o   o   o   o   o   o
|   |   |   |   |   |   |           |       |   |   |   |   |   |
o   o   o   o   o---o   o---o   o   o---o---o---o   o   o   o   o
|   |   |   |   |       |       |   |           |   |   |   |   |
o   o   o   o   o   o---o   o---o---o   o---o   o   o   o   o   o
|   |   |   |       |                       |   |   |   |   |   |
o   o   o   o   o---o---o---o---o---o---o---o   o   o   o   o   o
|       |   |                                   |       |   |   |
o   o   o   o---o---o---o---o---o---o---o---o---o---o---o   o   o
|   |   |                                                   |   |
o   o   o---o---o---o---o---o---o---o---o---o---o---o---o---o   o
| S |                                                           |
o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o)";

std::string apec2023 = R"(o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o
|                                                               |
o   o---o---o---o---o---o---o---o---o---o---o---o---o---o---o   o
|   |               |               |                           |
o   o   o---o---o---o   o---o---o   o   o---o---o---o---o---o   o
|   |   |           |       |       |               |           |
o   o   o   o---o   o   o---o   o---o---o---o---o   o---o   o   o
|   |   |   |           |       |                   |       |   |
o   o   o   o---o---o---o---o   o---o---o   o---o---o   o---o   o
|   |   |                   |   |       |   |               |   |
o   o   o---o---o---o---o   o   o   o   o   o---o   o---o---o   o
|   |   |                   |       |       |       |       |   |
o   o   o   o---o---o---o---o---o---o---o---o   o---o   o   o   o
|   |   |               |               |       |       |   |   |
o   o   o   o---o---o   o   o---o---o---o   o---o   o---o   o   o
|   |   |   |           |     G   G |           |   |       |   |
o   o   o   o---o   o---o   o   o   o   o---o---o   o   o---o   o
|   |   |   |       |       | G   G |               |       |   |
o   o   o---o   o---o   o   o---o---o---o---o   o---o---o   o   o
|   |   |       |       |                   |   |           |   |
o   o   o   o---o   o---o   o   o---o   o---o---o   o---o---o   o
|   |   |   |       |   |   |               |   |           |   |
o   o   o   o   o---o   o   o---o   o---o   o   o---o---o   o   o
|   |   |   |           |       |           |               |   |
o   o   o   o---o---o   o   o---o---o   o---o---o   o---o---o   o
|   |   |               |       |           |       |       |   |
o   o   o   o---o---o---o---o---o---o---o   o   o---o   o   o   o
|       |   |                               |           |   |   |
o   o   o   o   o---o---o---o---o---o---o---o---o---o---o   o   o
|   |   |   |                                               |   |
o   o   o---o---o---o---o---o---o---o---o---o---o---o---o---o   o
| S |                                                           |
o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o)";

std::string apec2019 = R"(o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o
|                                                               |
o   o---o---o---o---o---o---o---o---o---o---o---o---o---o---o   o
|   |                                                           |
o   o   o---o---o---o---o---o---o---o---o---o---o---o---o---o   o
|   |   |   |               |               |               |   |
o   o   o   o---o   o---o   o---o   o---o   o---o   o---o   o   o
|   |   |       |       |       |       |       |       |   |   |
o   o   o   o   o---o   o---o   o---o   o---o   o---o   o   o   o
|   |   |   |       |       |       |       |           |   |   |
o   o   o   o---o   o---o   o---o   o---o   o---o---o---o   o   o
|   |   |       |           |               |               |   |
o   o   o   o---o---o---o---o---o---o---o---o   o   o---o   o   o
|   |   |   |       |                           |   |       |   |
o   o   o   o   o   o   o---o---o---o---o---o---o---o   o---o   o
|   |   |   |   |   |         G   G |                   |       |
o   o   o   o   o   o---o---o   o   o   o---o---o---o---o   o   o
|   |   |   |   |           | G   G |           |           |   |
o   o   o   o   o---o---o   o---o---o---o---o   o   o---o---o   o
|   |   |   |       |                   |       |           |   |
o   o   o   o---o   o   o---o---o---o   o   o---o---o---o   o   o
|   |   |   |       |   |           |       |               |   |
o   o   o   o   o---o   o   o---o   o---o---o   o---o---o   o   o
|   |   |   |           |       |           |           |   |   |
o   o   o   o---o---o   o---o   o---o---o   o---o---o   o   o   o
|   |   |   |           |       |           |       |   |   |   |
o   o   o   o   o---o---o---o   o   o---o---o   o   o   o   o   o
|       |   |                   |               |       |   |   |
o   o   o   o---o---o---o---o---o---o---o---o---o---o---o   o   o
|   |   |                                                   |   |
o   o   o---o---o---o---o---o---o---o---o---o---o---o---o---o   o
| S |                                                           |
o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o)";

std::string alljapan_044_2023_exp_fin = R"(o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o
|       |                                                       |
o   o   o   o---o---o---o---o---o   o---o   o---o---o---o---o   o
|   |       |                   |       |   |               |   |
o   o---o---o   o---o---o---o   o---o   o   o   o---o---o   o   o
|               |           |       |   |           |   |   |   |
o   o---o---o---o   o---o   o   o---o   o   o---o   o   o   o   o
|   |       |               |       |   |           |   |   |   |
o   o   o   o   o---o---o---o---o   o   o---o---o---o   o   o   o
|   |   |   |   |               |   |   |               |   |   |
o   o   o   o   o   o---o---o---o   o   o   o---o   o   o   o   o
|   |   |   |   |   |                   |   |       |   |   |   |
o   o   o   o   o   o---o---o---o---o---o---o   o---o   o   o   o
|   |   |   |       |                   |       |       |   |   |
o   o---o   o---o   o   o---o---o---o   o   o---o   o   o   o   o
|   |   |   |   |   |   |   | G   G |       |       |       |   |
o   o   o   o   o   o   o   o   o   o   o---o---o---o---o   o   o
|           |   |   |   |   | G   G     |               |   |   |
o   o   o   o   o   o   o   o   o---o---o   o---o   o   o   o   o
|   |   |   |   |   |   |               |   |       |   |   |   |
o   o   o   o   o   o   o---o---o---o   o   o   o   o   o   o   o
|   |   |           |               |   |       |   |   |       |
o   o---o---o---o---o---o---o---o   o   o   o---o   o   o   o   o
|                   |               |           |   |       |   |
o   o   o---o---o   o   o---o---o---o---o---o---o   o---o   o   o
|   |   |       |   |                   |       |           |   |
o   o   o   o   o   o---o---o---o---o   o   o---o---o---o---o   o
|       |   |       |                   |                       |
o   o   o   o---o---o   o---o---o---o---o---o---o   o   o   o   o
|   |   |       |                                   |   |   |   |
o   o---o   o---o---o---o---o---o---o---o---o   o   o---o---o   o
| S |                                           |               |
o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o)";

std::string minos24_a = R"(o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o
|                                                               |
o   o---o---o---o---o---o---o---o---o---o---o---o---o---o---o   o
|   |                                                           |
o   o---o---o---o---o   o---o   o---o---o---o---o---o---o---o   o
|                   |       |       |   |       |       |       |
o   o---o---o---o   o   o   o   o   o   o---o   o   o---o   o   o
|       |           |   |       |                           |   |
o   o---o   o---o---o   o---o   o   o---o---o---o   o---o   o   o
|   |       |       |   |                                   |   |
o---o   o---o---o   o   o   o   o---o---o---o---o   o---o---o   o
|                   |       |                               |   |
o   o---o---o   o---o   o---o   o---o---o---o---o---o---o---o   o
|               |                                           |   |
o   o   o   o   o   o   o   o   o---o---o---o---o---o---o   o   o
|   |   |   |   |   |   |   | G   G |                       |   |
o   o   o   o   o   o   o   o   o   o   o---o---o---o---o---o   o
|   |   |   |   |           | G   G |                       |   |
o   o   o   o   o---o---o---o---o---o---o---o---o   o---o   o   o
|   |   |           |                                   |       |
o   o   o   o   o   o   o---o---o---o---o   o---o   o   o   o   o
|   |   |   |   |   |                               |       |   |
o   o   o   o   o   o   o   o   o---o---o---o---o---o   o   o   o
|   |       |   |   |   |   |   |                       |       |
o   o   o   o   o   o   o   o   o   o---o   o---o   o---o   o   o
|   |   |   |   |                   |       |               |   |
o   o   o   o   o   o   o---o   o   o   o---o   o---o   o---o   o
|   |   |       |   |   |       |               |               |
o   o   o   o   o   o   o   o---o   o---o   o---o   o---o---o   o
|   |   |   |   |                                               |
o   o   o   o   o   o---o---o   o---o   o---o   o---o   o---o   o
| S |                                                           |
o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o)";

std::string minos24_b = R"(o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o
|   |                   |                                       |
o   o   o---o   o   o   o   o---o---o---o---o---o---o---o   o   o
|               |   |   |                                   |   |
o   o---o---o---o   o   o   o---o---o---o   o   o---o---o   o   o
|   |           |   |   |                   |                   |
o   o   o---o   o   o   o---o   o---o---o   o   o---o---o---o   o
|               |   |       |                   |           |   |
o---o---o   o---o   o---o   o---o---o---o---o   o   o   o   o   o
|               |                           |       |   |       |
o   o---o---o   o---o---o---o---o---o---o   o---o   o   o   o   o
|           |   |                               |       |   |   |
o---o---o   o   o   o---o---o   o   o---o---o   o---o   o   o   o
|       |   |   |           |   |                       |   |   |
o   o   o   o   o---o---o   o   o---o---o---o---o---o---o   o   o
|   |   |   |           |   | G   G |                       |   |
o   o   o   o---o---o   o   o   o   o   o   o---o   o   o---o   o
|                   |   |   | G   G |   |           |       |   |
o   o   o   o   o   o   o   o---o---o   o   o   o   o   o   o   o
|   |   |   |   |   |   |                   |   |   |   |       |
o   o   o   o   o   o   o---o---o---o   o   o   o   o   o---o   o
|   |   |   |   |       |       |       |   |   |   |   |       |
o   o   o   o   o   o   o   o   o   o   o   o   o   o   o   o   o
|   |   |   |   |   |   |   |   |   |   |   |   |   |       |   |
o   o   o   o   o   o   o   o   o   o   o   o   o   o   o   o   o
|       |           |           |           |           |   |   |
o   o   o---o   o---o   o---o---o---o---o   o---o   o---o   o   o
|   |                           |                               |
o   o   o---o---o---o---o   o   o   o---o---o---o---o   o---o   o
|   |   |                   |   |                               |
o   o   o   o---o---o---o---o   o---o---o---o---o   o---o---o   o
| S |                                                           |
o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o)";

std::string minos24_c = R"(o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o
|                                                       |       |
o   o---o---o---o   o---o   o   o---o---o---o   o---o   o   o   o
|                           |   |               |       |   |   |
o   o---o---o   o---o---o   o   o   o---o   o   o   o---o---o   o
|                           |           |   |   |       |       |
o   o---o   o---o---o---o   o   o---o---o   o   o---o   o   o---o
|                                       |   |   |               |
o   o---o---o---o---o---o   o---o---o---o   o   o---o---o---o   o
|                       |   |           |                       |
o   o---o---o---o---o   o---o   o---o   o   o   o---o---o   o   o
|   |                   |           |       |   |           |   |
o   o   o---o---o---o---o   o---o   o---o---o   o   o---o   o   o
|   |   |                                   |   |   |   |   |   |
o   o   o   o---o---o---o   o   o---o---o   o   o   o---o   o   o
|   |   |               |   | G   G |   |   |   |           |   |
o   o   o---o---o---o   o   o   o   o   o   o   o---o---o   o   o
|   |                   |   | G   G |   |   |   |               |
o   o---o---o---o---o---o   o---o---o   o   o   o---o   o---o   o
|                       |               |       |               |
o   o---o---o---o   o   o---o---o---o---o---o---o   o   o   o   o
|   |               |   |   |               |       |   |   |   |
o   o   o---o---o   o   o   o   o   o---o---o   o   o   o   o   o
|   |           |   |       |   |               |   |   |       |
o---o---o---o   o   o   o   o   o---o---o---o   o   o   o   o   o
|               |   |   |   |               |   |   |       |   |
o   o---o---o   o   o   o   o   o   o---o   o   o   o   o   o   o
|   |           |           |   |               |       |   |   |
o   o   o---o---o   o   o   o   o---o---o---o   o   o   o   o   o
|   |               |   |                   |   |   |   |   |   |
o   o---o---o---o   o   o   o   o---o---o   o   o   o   o   o   o
| S |                       |                                   |
o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o)";

std::string aamc24 = R"(o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o
|                       |           |                           |
o   o---o   o---o   o   o   o   o   o---o---o   o---o   o   o   o
|       |       |   |       |   |           |   |   |   |   |   |
o   o---o   o---o   o   o---o   o---o   o   o   o   o---o   o   o
|   |   |   |   |   |           |       |                   |   |
o   o   o   o   o   o   o---o---o---o   o   o   o   o---o   o   o
|                       |           |       |   |   |           |
o   o---o---o   o---o   o   o---o   o---o   o---o---o   o---o   o
|   |   |   |   |   |                   |                       |
o   o   o   o   o   o   o   o---o---o   o---o---o---o   o   o---o
|   |   |   |   |       |           |       |           |       |
o   o   o   o   o---o   o   o   o   o   o   o   o---o   o   o   o
|                       |   |   |       |                   |   |
o   o---o   o   o   o---o   o---o   o   o   o---o---o   o   o   o
|       |   |   |   |       | G   G |           |       |   |   |
o   o---o   o---o   o---o   o   o   o   o   o   o   o---o   o   o
|   |           |   |   |   | G   G |   |   |               |   |
o   o---o   o   o   o   o---o---o---o   o---o   o---o   o   o   o
|           |               |   |       |       |       |       |
o   o   o   o   o---o---o   o   o---o   o---o---o---o   o   o---o
|   |   |   |   |       |       |   |                           |
o---o   o---o   o   o---o   o---o   o---o---o   o---o---o   o---o
|               |       |       |   |   |                       |
o   o---o---o   o   o---o---o   o   o   o---o   o---o   o---o   o
|       |   |           |       |               |           |   |
o   o---o   o   o   o   o   o---o   o   o---o   o---o   o---o   o
|               |   |               |       |   |           |   |
o   o---o---o   o   o   o---o---o---o---o   o   o---o   o   o   o
|       |                                   |   |       |       |
o   o   o   o---o   o   o   o   o---o---o---o   o---o---o---o   o
| S |       |       |   |   |                                   |
o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o)";

std::string aamc23 = R"(o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o
|               |       |           |                           |
o   o---o   o   o   o   o   o   o   o---o---o---o---o   o   o   o
|       |   |       |       |   |           |   |   |   |   |   |
o   o---o---o   o---o---o---o   o---o   o   o   o   o---o   o   o
|               |               |       |                   |   |
o   o---o   o   o---o   o---o---o---o   o   o   o   o---o   o   o
|       |   |   |   |       |       |       |   |   |           |
o   o---o---o   o   o---o   o---o   o---o   o---o---o   o---o   o
|                       |       |       |                       |
o   o---o---o   o---o   o---o   o---o   o---o---o---o   o   o---o
|           |       |       |       |       |           |       |
o   o---o---o   o   o   o   o---o   o   o   o   o---o   o   o   o
|           |   |       |       |       |                   |   |
o   o---o---o   o   o---o   o---o   o   o   o---o---o   o   o   o
|                   |       | G   G |           |       |   |   |
o   o---o---o   o---o---o---o   o   o   o   o   o   o---o   o   o
|   |       |   |           | G   G |   |   |               |   |
o   o   o---o   o   o   o---o---o---o   o---o   o---o   o   o   o
|               |   |   |   |           |               |       |
o   o---o   o   o   o   o   o   o---o   o---o---o---o   o   o---o
|   |   |   |   |   |   |       |   |                           |
o   o   o---o   o   o   o   o---o   o---o---o   o---o---o   o---o
|               |   |   |       |   |   |                       |
o   o   o   o   o   o   o---o   o   o   o---o   o---o   o---o   o
|   |   |   |   |   |   |       |   |           |               |
o   o---o---o   o   o   o   o---o   o   o---o   o---o   o---o   o
|               |   |   |                   |       |       |   |
o   o---o---o   o   o   o---o---o---o---o   o   o   o   o   o   o
|       |           |                       |   |       |       |
o   o   o   o---o   o   o   o   o---o---o---o   o---o---o---o   o
| S |       |       |   |   |                                   |
o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o---o)";
//std::string apec2023 = R"()";

#endif /* MAZE_COLLECTION_H_ */
