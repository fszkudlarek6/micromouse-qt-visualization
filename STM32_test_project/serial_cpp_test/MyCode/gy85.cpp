/*
 * gy85.cpp
 *
 *  Created on: Jun 15, 2024
 *      Author: phelipe
 */

#include <gy85.h>
#include <main.h>

extern I2C_HandleTypeDef hi2c2;

bool itg3205_init() {
	uint8_t temp_data;
	HAL_StatusTypeDef ret_itg_connected = HAL_I2C_IsDeviceReady(&hi2c2,
	ITG_ADDRESS_W, 1, 100);
	if (ret_itg_connected == HAL_OK) {
		//TODO: add debug info
	} else {
		return 0;
	}

	temp_data = ITG_FULL_SCALE;
	HAL_StatusTypeDef ret_itg_configured = HAL_I2C_Mem_Write(&hi2c2,
	ITG_ADDRESS_W,
	ITG_REG_CONF_SCALE, 1, &temp_data, 1, 100);
	if (ret_itg_configured == HAL_OK) {
		//TODO: add debug info
	} else {
		return 0;
	}
	temp_data = ITG_POW_MAG_DEFAULT;
	HAL_StatusTypeDef ret_itg_configured_pwr = HAL_I2C_Mem_Write(&hi2c2,
	ITG_ADDRESS_W,
	ITG_REG_POW_MAG, 1, &temp_data, 1, 100);
	if (ret_itg_configured_pwr == HAL_OK) {
		//TODO: add debug info
	} else {
		return 0;
	}
	return 1;
}

bool itg3205_read(float *return_data) {
	uint8_t data[8];
	HAL_StatusTypeDef ret_data_read = HAL_I2C_Mem_Read(&hi2c2, ITG_ADDRESS_R,
	ITG_REG_DATA, 1, data, 8, 100);
	if (ret_data_read == HAL_OK) {
		int temperature = (int16_t) ((data[0] << 8) + data[1]);
		temperature = 35 + ((double) (temperature + 13200)) / 280;
		uint16_t gyro[3];
		uint16_t gyro_offset[3] = {120,20,93};
		float gyro_deg_per_sec[3];
		for (int i = 0; i < 3; ++i) {
			gyro[i] = (int16_t) ((data[2 + 2 * i] << 8) + data[3 + 2 * i]);
			gyro[i] += gyro_offset[i];
			gyro_deg_per_sec[i] = (float) gyro[i] / 14.375;
			(return_data)[i] = gyro_deg_per_sec[i];
		}
		return 1;
	} else {
		return 0;
	}
}

bool itg3205_read_chat(float *return_data) {
	uint8_t data[8];
	HAL_StatusTypeDef ret_data_read = HAL_I2C_Mem_Read(&hi2c2, ITG_ADDRESS_R, ITG_REG_DATA, 1, data, 8, 100);
	if (ret_data_read == HAL_OK) {
		int16_t temperature = (int16_t) ((data[0] << 8) + data[1]);
		temperature = 35 + ((double) (temperature + 13200)) / 280; // Jeśli potrzebujesz obliczenia temperatury
		int16_t gyro[3];
		int16_t gyro_offset[3] = {38, -200, -56};
		float gyro_deg_per_sec[3];
		for (int i = 0; i < 3; ++i) {
			gyro[i] = (int16_t) ((data[2 + 2 * i] << 8) + data[3 + 2 * i]);
			gyro[i] += gyro_offset[i]; // Jeśli offset powinien być odejmowany
			gyro_deg_per_sec[i] = (float) gyro[i] / 14.375;
			return_data[i] = gyro_deg_per_sec[i];
		}
		return 1;
	} else {
		return 0;
	}
}

bool hmc8883L_init() {
	//uint8_t temp_data;
	HAL_StatusTypeDef ret_hmc_connected = HAL_I2C_IsDeviceReady(&hi2c2,
	HMC_ADDRESS_W, 1, 100);
	if (ret_hmc_connected == HAL_OK) {
		//TODO: add debug info
	} else {
		return 0;
	}

	uint8_t temp_data[3] = { HMC_CONF_A, HMC_CONF_B_DEFAULT, HMC_MODE_CONT };
	HAL_StatusTypeDef ret_hmc_configured = HAL_I2C_Mem_Write(&hi2c2,
	HMC_ADDRESS_W,
	HMC_REG_CONF_A, 1, temp_data, 3, 100);
	if (ret_hmc_configured == HAL_OK) {
		//TODO: add debug info
	} else {
		return 0;
	}
	return 1;
}

bool hmc8883L_read(float *return_data) {
	uint8_t data[6];
	int16_t mag[3];
	uint8_t status;
	HAL_StatusTypeDef ret_status_read = HAL_I2C_Mem_Read(&hi2c2, HMC_ADDRESS_R,
	HMC_REG_STATUS, 1, &status, 1, 100);
	if (ret_status_read == HAL_OK) {
		if ((status & 1) == 1) {
			HAL_StatusTypeDef ret_data_read = HAL_I2C_Mem_Read(&hi2c2,
			HMC_ADDRESS_R, HMC_REG_DATA, 1, data, 6, 100);
			if (ret_data_read == HAL_OK) {
				float gauss[3];
				for (int i = 0; i < 3; ++i) {
					mag[i] = (int16_t) ((data[2 * i] << 8) | data[2 * i + 1]);
					gauss[i] = (float) mag[i] / (float) GAIN_LSB_PER_GAUSS;
					(return_data)[i] = gauss[i];
				}
				return 1;
			} else {
				//TODO: add debug info
				return 0;
			}
		} else {
			//TODO: add debug info
			return 0;
		}
	} else {
		//TODO: add debug info
		return 0;
	}
}

bool adxl345_init() {
	uint8_t temp_data;
	HAL_StatusTypeDef ret_adxl_connected = HAL_I2C_IsDeviceReady(&hi2c2,
	ADX_ADDRESS_W, 1, 100);
	if (ret_adxl_connected == HAL_OK) {
		//TODO: add debug info
	} else {
		return 0;
	}
	temp_data = ADX_SET_MEASURE;
	HAL_StatusTypeDef ret_adx_configured = HAL_I2C_Mem_Write(&hi2c2,
	ADX_ADDRESS_W, ADX_REG_PWR_CTL, 1, &temp_data, 1, 100);
	if (ret_adx_configured == HAL_OK) {
		//TODO: add debug info
	} else {
		return 0;
	}
	temp_data = ADX_RANGE_4G;
	ret_adx_configured = HAL_I2C_Mem_Write(&hi2c2,
	ADX_ADDRESS_W,
	ADX_REG_RANGE, 1, &temp_data, 1, 100);
	if (ret_adx_configured == HAL_OK) {
		//TODO: add debug info
	} else {
		return 0;
	}
	return 1;
}

bool adxl345_read(float *return_data) {
	uint8_t data[6];
	int16_t acc[3];
	HAL_StatusTypeDef ret_data_read = HAL_I2C_Mem_Read(&hi2c2, ADX_ADDRESS_R,
	ADX_REG_DATA, 1, data, 6, 100);
	if (ret_data_read == HAL_OK) {
		float g_force[3];
		for (int i = 0; i < 3; ++i) {
			acc[i] = (int16_t) ((data[2 * i + 1] << 8) | data[2 * i]);
			g_force[i] = acc[i] / (float) ADX_DIVIDER;
			(return_data)[i] = g_force[i];
		}
		return 1;
	} else {
		return 0;
	}
}
