/*
 * board.h
 *
 *  Created on: Apr 28, 2024
 *      Author: phelipe
 */

#ifndef BOARD_H_
#define BOARD_H_

#include <string>
#include <sstream>
#include <cstring>

#define LAB_SIZE 16
//magic number, to ensure, that each weight takes 2 digits to write
//CAUTIOUS -- it should be substracted in the app
#define FLOODFILL_SENDING_ADDITION 10

class board {
	int floodFillWeightsTab[LAB_SIZE][LAB_SIZE];
public:
	board();

	virtual ~board();

	char boardTab[LAB_SIZE][LAB_SIZE];
	void addTopWall(int x, int y);
	void addRightWall(int x, int y);
	void addBotWall(int x, int y);
	void addLeftWall(int x, int y);
	void markVisited(int x, int y);

	bool hasTopWall(int x, int y);
	bool hasRightWall(int x, int y);
	bool hasBotWall(int x, int y);
	bool hasLeftWall(int x, int y);
	bool wasVisited(int x, int y);


	void get_lab_data(char ret[][800]);

	void parseMicromouseMazeFromString(const std::string& mazeString);


	void setFloodFillWeight(int x, int y, int weight);

    int getFloodFillWeight(int x, int y);

    int getLowestNeighbiourWeight(int x, int y);

	void getNeighbourWithLowestWeight(int *x, int *y, int *robotOrientation);

    void calculateManhattanDistanceForCell(int x, int y);

    void floodCell(int x, int y);

    void calculateManhattanDistanceForNeighbours(int x, int y);

    void floodNeighbours(int x, int y);
    //calculates the Manhattan distance for each cell and writes it
    void initialFloodManhattanDistance();

	void get_floodfill_data(char ret[][1200]);
};

#endif /* BOARD_H_ */
