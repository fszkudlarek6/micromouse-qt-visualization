/*
 * board.cpp
 *
 *  Created on: Apr 28, 2024
 *      Author: phelipe
 */

#include "board.h"

board::board() {
	for(int i = 0; i<LAB_SIZE; ++i){
		for(int j = 0; j<LAB_SIZE; ++j){
				this->boardTab[i][j] = 32;
				//setting to some high value to perform actual flood fill
                floodFillWeightsTab[j][i] = 600;
			}
		}
	for(int i = 0; i<LAB_SIZE; ++i){
		this->addBotWall(i, 0);
		this->addTopWall(i, LAB_SIZE-1);
	}
	for(int j = 0; j<LAB_SIZE; ++j){
			this->addLeftWall(0, j);
			this->addRightWall(LAB_SIZE-1, j);
	}
	
    initialFloodManhattanDistance();
}

board::~board() {
	// TODO Auto-generated destructor stub
}

//for next 4 functions: 0-no wall, 1-wall
//first least significant bit is top wall
void board::addTopWall(int x, int y){
	this->boardTab[x][y] = (this->boardTab[x][y]|1);
}
//second least significant bit is right wall
void board::addRightWall(int x, int y){
	this->boardTab[x][y] = (this->boardTab[x][y]|2);
}
//third least significant bit is bottom wall
void board::addBotWall(int x, int y){
	this->boardTab[x][y] = (this->boardTab[x][y]|4);
}
//fourth least significant bit is left wall
void board::addLeftWall(int x, int y){
	this->boardTab[x][y] = (this->boardTab[x][y]|8);
}
//fifth least significant bit says if cell was visited 0-wasn't, 1-was
void board::markVisited(int x, int y){
	this->boardTab[x][y] = (this->boardTab[x][y]|16);
}

bool board::hasTopWall(int x, int y){
    return((this->boardTab[x][y]&1) == 1);
}
//second least significant bit is right wall
bool board::hasRightWall(int x, int y){
    return((this->boardTab[x][y]&2) == 2);
}
//third least significant bit is bottom wall
bool board::hasBotWall(int x, int y){
    return((this->boardTab[x][y]&4) == 4);
}
//fourth least significant bit is left wall
bool board::hasLeftWall(int x, int y){
    return ((this->boardTab[x][y]&8) == 8);
}
//fifth least significant bit says if cell was visited 0-wasn't, 1-was
bool board::wasVisited(int x, int y){
    return ((this->boardTab[x][y]&16) == 16);
}

//outer loop (i) iterates through Y axis
//inner loop (j) iterates through X axis
void board::get_lab_data(char ret[][800]){
	std::string ret_string;
	ret_string += "LAB: ";
	for(int i = 0; i < LAB_SIZE; ++i){
		for(int j = 0; j < LAB_SIZE; ++j){
			ret_string += std::to_string(this->boardTab[j][i]);
			//ret_string += this->boardTab[j][i];
		}
	}
	ret_string += " ";
	std::strcpy(*ret, ret_string.c_str());
}

void board::parseMicromouseMazeFromString(const std::string& mazeString) {
    const int MAX_LINES = 64;       // Maximum number of lines supported
    const int MAX_LINE_LENGTH = 256; // Maximum length of each line
    const int VERTICAL_LINE_LENGTH = 66;
    const int HORIZONTAL_LINE_LENGTH = 34;

    char mazeLines[MAX_LINES][VERTICAL_LINE_LENGTH]; // Fixed-size array for storing lines
    int lineCount = 0;

    // Split the maze string into lines
    size_t start = 0, end;
    while ((end = mazeString.find('\n', start)) != std::string::npos && lineCount < MAX_LINES) {
        size_t length = end - start;
        if (length >= MAX_LINE_LENGTH) {
            return;
        }

        std::strncpy(mazeLines[lineCount], mazeString.c_str() + start, length);
        mazeLines[lineCount][length] = '\0'; // Null-terminate the line
        lineCount++;
        start = end + 1;
    }

    // Handle the last line if it does not end with '\n'
    if (start < mazeString.size() && lineCount < MAX_LINES) {
        size_t length = mazeString.size() - start;
        if (length >= MAX_LINE_LENGTH) {
            return;
        }
        std::strncpy(mazeLines[lineCount], mazeString.c_str() + start, length);
        mazeLines[lineCount][length] = '\0'; // Null-terminate the line
        lineCount++;
    }
    
    char mazeLinesTransposed[VERTICAL_LINE_LENGTH][MAX_LINES];
    for (int i = 0; i < MAX_LINES; ++i) {
        for (int j = 0; j < VERTICAL_LINE_LENGTH; ++j) {
            mazeLinesTransposed[j][i] = mazeLines[i][j];
        }
    }
    
    char verticalLines[LAB_SIZE][VERTICAL_LINE_LENGTH];
    char horizontalLines[LAB_SIZE][HORIZONTAL_LINE_LENGTH];
    
    memset(verticalLines, 0, sizeof(verticalLines));
    memset(horizontalLines, 0, sizeof(horizontalLines));

    
    for(int i = 0; i< LAB_SIZE; ++i){
        std::strcpy(verticalLines[i], mazeLines[i*2 + 1]);
    }

    for(int i = 1; i < LAB_SIZE+1; ++i){
        std::strcpy(horizontalLines[i-1], mazeLinesTransposed[i*4 - 2]);
    }
    
    std::strcpy(horizontalLines[LAB_SIZE], mazeLinesTransposed[LAB_SIZE*4 - 2]);
    
    //populate vertical lines
    for(int i = 0; i< LAB_SIZE; ++i){
        std::strcpy(verticalLines[i], mazeLines[i*2 + 1]);
    }
        
    bool verticalWalls[LAB_SIZE][LAB_SIZE+1];
    
    for(int i =0; i<LAB_SIZE; ++i){
        for(int j=0; j<LAB_SIZE+1; ++j){
            if(verticalLines[LAB_SIZE-1-i][j*4] == '|'){
                verticalWalls[i][j] = 1;
            }
            else{
                verticalWalls[i][j] = 0;
            }
        }
    }    
    
    bool horizontalWalls[LAB_SIZE][LAB_SIZE+1];
    
    for(int i =0; i<LAB_SIZE; ++i){
        // std::cout << "\nLine:   "<< std::string(horizontalLines[i]);
        for(int j=0; j<LAB_SIZE+1; ++j){
            if(horizontalLines[i][2*LAB_SIZE - j*2] == '-'){
                horizontalWalls[i][j] = 1;
            }
            else{
                horizontalWalls[i][j] = 0;
            }
        }
    }
    for(int i=0; i< LAB_SIZE; ++i){
        for(int j=0; j< LAB_SIZE; ++j){
            if(verticalWalls[j][i] == 1){
                addLeftWall(i, j);
            }
            if(verticalWalls[j][i+1] == 1){
                addRightWall(i, j);
            }
            if(horizontalWalls[i][j] ==1){
                addBotWall(i, j);
            }
            if(horizontalWalls[i][j+1] ==1){
                addTopWall(i, j);
            }
        }
    }

}

void board::setFloodFillWeight(int x, int y, int weight){
	this->floodFillWeightsTab[x][y] = weight;
}

int board::getFloodFillWeight(int x, int y){
    return this->floodFillWeightsTab[x][y];
}

int board::getLowestNeighbiourWeight(int x, int y){
    //first posiotion -top; second - right; third - bot, fourth - left
    int weights[4] = {600, 600, 600, 600};
    //check, which cells can be entered if cell was visited
    if(wasVisited(x, y)){
        if(!hasTopWall(x, y)){
            weights[0] = getFloodFillWeight(x, y+1);
        }
        if(!hasRightWall(x, y)){
            weights[1] = getFloodFillWeight(x+1, y);
        }
        if(!hasBotWall(x, y)){
            weights[2] = getFloodFillWeight(x, y-1);
        }
        if(!hasLeftWall(x, y)){
            weights[3] = getFloodFillWeight(x-1, y);
        }
    }
    //assume no walls if cell wasn't visited
    else {
        if(y < (LAB_SIZE-1)){
            weights[0] = getFloodFillWeight(x, y+1);
        }
        if(x < (LAB_SIZE-1)){
            weights[1] = getFloodFillWeight(x+1, y);
        }
        if(y > 0){
            weights[2] = getFloodFillWeight(x, y-1);
        }
        if(x > 0){
            weights[3] = getFloodFillWeight(x-1, y);
        }
    }
    int lowest_weight = 600;
    for(uint8_t i =0; i<4; ++i){
        if(weights[i] < lowest_weight){
            lowest_weight = weights[i];
        }
    }
    return lowest_weight;
}

void board::getNeighbourWithLowestWeight(int *x, int *y, int *robotOrientation){
    //first posiotion -top; second - right; third - bot, fourth - left
    int weights[4] = {600, 600, 600, 600};
    //check, which cells can be entered if cell was visited
    if(!hasTopWall(*x, *y)){
        weights[0] = getFloodFillWeight(*x, *y+1);
    }
    if(!hasRightWall(*x, *y)){
        weights[1] = getFloodFillWeight(*x+1, *y);
    }
    if(!hasBotWall(*x, *y)){
        weights[2] = getFloodFillWeight(*x, *y-1);
    }
    if(!hasLeftWall(*x, *y)){
        weights[3] = getFloodFillWeight(*x-1, *y);
    }
    int lowest_weight = 600;
    uint8_t position;
    for(uint8_t i =0; i<4; ++i){
        if(weights[i] < lowest_weight){
            lowest_weight = weights[i];
            position = i;
        }
    }
    switch (position)
    {
    case 0:
        *y += 1;
        *robotOrientation = 90;
        break;
    case 1:
        *x += 1;
        *robotOrientation = 0;
        break;
    case 2:
        *y -= 1;
        *robotOrientation = 270;
        break;
    case 3:
        *x -= 1;
        *robotOrientation = 180;
        break;
    }
}

void board::calculateManhattanDistanceForCell(int x, int y){
    int lowestNeighbourWeight = getLowestNeighbiourWeight(x, y);
    if(wasVisited(x, y)){
        if((lowestNeighbourWeight+1) < getFloodFillWeight(x, y)){
            setFloodFillWeight(x, y, lowestNeighbourWeight+1);
            calculateManhattanDistanceForNeighbours(x, y);
        }
    }
    else {
        if((lowestNeighbourWeight+1) < getFloodFillWeight(x, y)){
            setFloodFillWeight(x, y, lowestNeighbourWeight+1);
            calculateManhattanDistanceForNeighbours(x, y);
        }
    }
}

void board::floodCell(int x, int y){
    int lowestNeighbourWeight = getLowestNeighbiourWeight(x, y);
    if(lowestNeighbourWeight != 0){
    	if(getFloodFillWeight(x, y) <= (lowestNeighbourWeight)){
    	        setFloodFillWeight(x, y, lowestNeighbourWeight+1);
    	        floodNeighbours(x, y);
    	    }
    }
}

void board::calculateManhattanDistanceForNeighbours(int x, int y){
    //flood adjacent cells, that can be entered from first cell
    if(wasVisited(x, y)){
        if(!hasTopWall(x, y)){
            calculateManhattanDistanceForCell(x, y+1);
        }
        if(!hasRightWall(x, y)){
            calculateManhattanDistanceForCell(x+1, y);
        }
        if(!hasBotWall(x, y)){
            calculateManhattanDistanceForCell(x, y-1);
        }
        if(!hasLeftWall(x, y)){
            calculateManhattanDistanceForCell(x-1, y);
        }
    }
    //assume no walls if cell wasn't visited
    else {
        if(y < (LAB_SIZE-1)){
            calculateManhattanDistanceForCell(x, y+1);
        }
        if(x < (LAB_SIZE-1)){
            calculateManhattanDistanceForCell(x+1, y);
        }
        if(y > 0){
            calculateManhattanDistanceForCell(x, y-1);
        }
        if(x > 0){
            calculateManhattanDistanceForCell(x-1, y);
        }
    }
}

void board::floodNeighbours(int x, int y){
    //flood adjacent cells, that can be entered from first cell
    if(wasVisited(x, y)){
        if(!hasTopWall(x, y)){
            floodCell(x, y+1);
        }
        if(!hasRightWall(x, y)){
            floodCell(x+1, y);
        }
        if(!hasBotWall(x, y)){
            floodCell(x, y-1);
        }
        if(!hasLeftWall(x, y)){
            floodCell(x-1, y);
        }
    }
    //assume no walls if cell wasn't visited
    else {
        if(y < (LAB_SIZE-1)){
            floodCell(x, y+1);
        }
        if(x < (LAB_SIZE-1)){
            floodCell(x+1, y);
        }
        if(y > 0){
            floodCell(x, y-1);
        }
        if(x > 0){
            floodCell(x-1, y);
        }
    }
}

void board::initialFloodManhattanDistance(){
    int higher_goal_dim = LAB_SIZE/2;
    int lower_goal_dim = higher_goal_dim - 1;
    //set goal cells weights to 0
    setFloodFillWeight(lower_goal_dim, lower_goal_dim, 0);
    setFloodFillWeight(lower_goal_dim, higher_goal_dim, 0);
    setFloodFillWeight(higher_goal_dim, higher_goal_dim, 0);
    setFloodFillWeight(higher_goal_dim, lower_goal_dim, 0);
    //flood goal cells
    calculateManhattanDistanceForCell(6, 7);
}

//outer loop (i) iterates through Y axis
//inner loop (j) iterates through X axis
void board::get_floodfill_data(char ret[][1200]){
	std::string ret_string;
	ret_string += "FLOOD: ";
	for(int i = 0; i < LAB_SIZE; ++i){
		for(int j = 0; j < LAB_SIZE; ++j){
			ret_string += std::to_string(this->floodFillWeightsTab[j][i]
                                        + FLOODFILL_SENDING_ADDITION);
		}
	}
	ret_string += " ";
	std::strcpy(*ret, ret_string.c_str());
}
