/*
 * gy85.h
 *
 *  Created on: Jun 15, 2024
 *      Author: phelipe
 */

#ifndef GY85_H_
#define GY85_H_

#include <stdbool.h>

//write addresses
#define ITG_ADDRESS_W 0b11010000
#define HMC_ADDRESS_W 0b00111100
#define ADX_ADDRESS_W 0b10100110
//read addresses
#define ITG_ADDRESS_R 0b11010001
#define HMC_ADDRESS_R 0b00111101
#define ADX_ADDRESS_R 0b10100111

#define ITG_REG_CONF_SCALE 22
#define ITG_REG_DATA 27
#define ITG_REG_POW_MAG 62

#define HMC_REG_CONF_A 0
#define HMC_REG_CONF_B 1
#define HMC_REG_MODE 2
#define HMC_REG_DATA 3
#define HMC_REG_STATUS 9

#define ADX_REG_PWR_CTL 0x2D
#define ADX_REG_RANGE 0x31
#define ADX_REG_DATA 0x32

#define ITG_FULL_SCALE 0b00011000
#define ITG_POW_MAG_ALL_ACTIV_Z_REF 0b00000011
#define ITG_POW_MAG_DEFAULT 0b00000000

//4 avg samples, 15Hz output rate, default measurement mode
#define HMC_CONF_A 0b01010000
//all same as default
#define HMC_CONF_B_DEFAULT 0b00100000
#define GAIN_LSB_PER_GAUSS 1090
#define HMC_MODE_CONT 0b00000000
#define HMC_MODE_SINGLE 0b00000001
#define HMC_MODE_IDLE 0b00000011

#define ADX_SET_MEASURE 0b00001000
#define ADX_RANGE_4G 0b00000001
#define ADX_DIVIDER 128

//all functions return 1 when success and 0 when fail
bool itg3205_init();
//returned value unit - degrees per second
bool itg3205_read(float *return_data);
bool itg3205_read_chat(float *return_data);

bool hmc8883L_init();
//returned value unit - gauss
bool hmc8883L_read(float *return_data);

bool adxl345_init();
//returned value unit - g
bool adxl345_read(float *return_data);

#endif /* GY85_H_ */
