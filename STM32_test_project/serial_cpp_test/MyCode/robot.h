/*
 * robot.h
 *
 *  Created on: May 18, 2024
 *      Author: phelipe
 */

#ifndef ROBOT_H_
#define ROBOT_H_
#include "board.h"

class robot {
	board *labBoard;
	//left bottom corner of the maze is (0,0)
	uint8_t position_x;
	uint8_t position_y;
	//0 is facing right
	//position is added CCW - as in Cartesian coordinate system
	int position_angle;

	bool movingUp;
	bool turning;
	bool turnCompleted;
	int col;
public:
	robot(board *currentBoard);
	virtual ~robot();

	void get_rob_data(char ret[][50]);
	void run_forest();
	void exploreColumnsStep();

	//returns false, if position is not withing baord boundries
	bool moveToPosition(uint8_t x, uint8_t y);
	//turns the robot and keeps the angle in <0, 360) range
	// above 0 is CCW turn, and below 0 is CW turn
	void turn(int turn_angle);

	//checks if the possition is within maze boundries
	bool isValidPosiotion(uint8_t x, uint8_t y);

	//methods telling wchich movement ways are open
	//they return true if movement is possible and false if not
	//coordinates of the cell, which robot can move to
	//are written to the arguments
	bool canGoFront(uint8_t *cell_to_move_position_x,
					uint8_t *cell_to_move_position_y);
	bool canGoRight(uint8_t *cell_to_move_position_x,
					uint8_t *cell_to_move_position_y);
	bool canGoLeft(uint8_t *cell_to_move_position_x,
					uint8_t *cell_to_move_position_y);
	bool canGoBack(uint8_t *cell_to_move_position_x,
					uint8_t *cell_to_move_position_y);
	
	void exploreLabirynthFront();

	void exploreLabirynthRight();

	void exploreLabirynthLeft();

	//prefers entering not visited cells
	void exploreLabirynthFrontModified();

	//prefers entering not visited cells
	void exploreLabirynthRightModified();

	//prefers entering not visited cells
	void exploreLabirynthLeftModified();


	void goToLeastFloodFillWeight();
};

#endif /* ROBOT_H_ */
