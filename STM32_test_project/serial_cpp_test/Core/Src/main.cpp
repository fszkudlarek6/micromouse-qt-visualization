/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.cpp
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2024 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
//#include "crc.h"
#include "i2c.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include "board.h"
#include "robot.h"
#include <gy85.h>
#include "maze_collection.h"
//#include <stdio>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
volatile int acc[3];
volatile int gyro[3];
volatile int mag[3];
volatile uint8_t lab[64];
board current_board;
int lab_iterator_x = 0;
int lab_iterator_y = 0;
bool button_clicked_flag = 0;

robot micro_mouse(&current_board);

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

#ifdef __cplusplus
extern "C" {
#endif
int _write(int file, char *ptr, int len) {
	//const uint8_t * ptr_uint = ptr;
	//const uint8_t * xd(ptr);
	HAL_UART_Transmit(&huart2, (const uint8_t*) ptr, len, 50);
	return len;
}
#ifdef __cplusplus
}
#endif

uint32_t calc_crc(char *data) {
	uint32_t crc_table[256];
	uint32_t crc = 0xFFFFFFFF; // Initial CRC value

	// Generate CRC32 lookup table
	for (int i = 0; i < 256; i++) {
		uint32_t crc_entry = i;
		for (int j = 0; j < 8; j++) {
			crc_entry =
					(crc_entry & 1) ?
							(crc_entry >> 1) ^ 0xEDB88320 : crc_entry >> 1;
		}
		crc_table[i] = crc_entry;
	}

	// Calculate CRC32
	while (*data != '\0') {
		crc = (crc >> 8) ^ crc_table[(crc ^ *data) & 0xFF];
		data++;
	}

	return ~crc; // Final CRC value (bitwise complement)
}

void inc_sens() {
	for (int i = 0; i < 3; ++i) {
		acc[i] += 3;
		acc[i] %= 100;
		gyro[i] += 2;
		gyro[i] %= 100;
		mag[i] += 4;
		mag[i] %= 100;
	}
}

void inc_lab() {
	lab_iterator_x %= 7;
	lab_iterator_y %= 7;

	current_board.markVisited(7, lab_iterator_y);
	++lab_iterator_x;
	if (lab_iterator_x == 8) {
		++lab_iterator_y;
	}
}

//writes sensor message to string given by ret
//mode==1 --acc, 2--gyro, 3--mag, 4--lab
void get_data(int mode, char ret[][50]) {
	//char ret[50];
	if (mode == 1) {
		sprintf(*ret, "ACC: %d %d %d ", acc[0], acc[1], acc[2]);
	} else if (mode == 2) {
		sprintf(*ret, "GYR: %d %d %d ", gyro[0], gyro[1], gyro[2]);
	} else if (mode == 3) {
		sprintf(*ret, "MAG: %d %d %d ", mag[0], mag[1], mag[2]);
	}
	/*else if (mode ==4){
	 //sprintf(ret, "LA1: %d%d%d ", lab[0], lab[1], lab[2]);
	 for(int i=0; i<32; ++i){
	 char c[1];
	 c[0] = (lab[i]);
	 strcat(ret, c);
	 }
	 //sprintf(ret, " ");
	 }
	 else if (mode ==5){
	 sprintf(ret, "LA2: ");
	 for(int i=32; i<6; ++i){
	 sprintf(ret, "%s", lab);
	 }
	 sprintf(ret, " ");
	 }*/
	//return ret;
}

void convert_float_gy85_data_to_int(float *data, int *return_data) {
	for (int i = 0; i < 3; ++i) {
		return_data[i] = static_cast<int>(100 * data[i]);
	}
}

bool get_data_gy85(int mode, char ret[][50]) {
	//char ret[50];
	if (mode == 1) {
		float acc_data_float[3];
		bool acc_read_ok = adxl345_read(acc_data_float);
		if (acc_read_ok) {
			int acc_data[3];
			convert_float_gy85_data_to_int(acc_data_float, acc_data);
			sprintf(*ret, "ACC: %d %d %d ", acc_data[0], acc_data[1],
					acc_data[2]);
			return 1;
		} else
			return 0;
	} else if (mode == 2) {
		float gyro_data_float[3];
		bool gyro_read_ok = itg3205_read_chat(gyro_data_float);
		if (gyro_read_ok) {
			int gyro_data[3];
			convert_float_gy85_data_to_int(gyro_data_float, gyro_data);
			sprintf(*ret, "GYR: %d %d %d ", gyro_data[0], gyro_data[1],
					gyro_data[2]);
			return 1;
		} else
			return 0;
	} else if (mode == 3) {
		float mag_data_float[3];
		bool mag_read_ok = hmc8883L_read(mag_data_float);
		if (mag_read_ok) {
			int mag_data[3];
			convert_float_gy85_data_to_int(mag_data_float, mag_data);
			sprintf(*ret, "MAG: %d %d %d ", mag_data[0], mag_data[1],
					mag_data[2]);
			return 1;
		} else
			return 0;
	} else
		return 0;
}

void write_sens() {
	for (int i = 1; i < 4; ++i) {
		char data[50];
		get_data(i, &data);
		//puts(data);
		printf("%s", data);
		uint32_t crc_value = calc_crc(data);
		printf("CRC: %lu \r\n", crc_value);
	}
	//printf("%s\r\n", lab);
	//puts(data);
}

void write_sens_gy85() {
	for (int i = 1; i < 4; ++i) {
		char data[50];
		if (get_data_gy85(i, &data)) {
			//puts(data);
			printf("%s", data);
			uint32_t crc_value = calc_crc(data);
			printf("CRC: %lu \r\n", crc_value);
		}
	}
}

void write_lab() {
	char data[800];
	//current_board.
	current_board.get_lab_data(&data);
	printf("%s", data);
	uint32_t crc_value = calc_crc(data);
	printf("CRC: %lu \r\n", crc_value);
}

void write_flood_fill() {
	char data[1200];
	//current_board.
	current_board.get_floodfill_data(&data);
	printf("%s", data);
	uint32_t crc_value = calc_crc(data);
	printf("CRC: %lu \r\n", crc_value);
}

void write_robot() {
	char data[50];
	//current_board.
	micro_mouse.get_rob_data(&data);
	printf("%s", data);
	uint32_t crc_value = calc_crc(data);
	printf("CRC: %lu \r\n", crc_value);
}
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void) {

	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_USART2_UART_Init();
	MX_I2C2_Init();
	//MX_CRC_Init();
	/* USER CODE BEGIN 2 */
	bool adx_init_ok = adxl345_init();
	bool itg_init_ok = itg3205_init();
	bool hmc_init_ok = hmc8883L_init();

	for (int i = 0; i < 3; ++i) {
		acc[i] = 50 + 20 * i;
		gyro[i] = 70 + 20 * i;
		mag[i] = 20 + 20 * i;
	}

	current_board.parseMicromouseMazeFromString(alljapan_044_2023_exp_fin);

	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1) {
		//write_sens();
//		write_sens_gy85();
//		HAL_Delay(200);
//		write_sens_gy85();
//		HAL_Delay(200);
//		write_sens_gy85();
//		HAL_Delay(200);
//		write_sens_gy85();
//		HAL_Delay(200);
//		write_sens_gy85();
//		HAL_Delay(200);
		HAL_Delay(100);
		write_lab();
		write_robot();
		write_flood_fill();
		//inc_sens();
		//micro_mouse.run_forest();
		if(button_clicked_flag){
			// micro_mouse.simulateAndExploreMazeStep();
			micro_mouse.exploreLabirynthFrontModified();
			//button_clicked_flag = 0;
		}
		else{
			HAL_Delay(2000);
		}
		HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
		//HAL_Delay(1000);
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	/** Configure the main internal regulator output voltage
	 */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1)
			!= HAL_OK) {
		Error_Handler();
	}

	/** Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure.
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 1;
	RCC_OscInitStruct.PLL.PLLN = 10;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}

	/** Initializes the CPU, AHB and APB buses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK) {
		Error_Handler();
	}
}

/* USER CODE BEGIN 4 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  if(GPIO_Pin == GPIO_PIN_13) {
    button_clicked_flag = 1;
  } else {
      __NOP();
  }
}
/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1) {
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
