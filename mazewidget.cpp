#include "mazewidget.h"

mazeWidget::mazeWidget(QWidget *parent)
    : QWidget{parent}
{}

mazeWidget::mazeWidget(board *boardToAdd)
{
    labBoard = boardToAdd;

    robotIcon = QPixmap(ROBOT_IMAGE_PATH);
    robotLabel.setParent(this);

    if (robotIcon.isNull()) {
        qDebug() << "              Failed to load image from path:" << ROBOT_IMAGE_PATH;
    }

    showTrace = 0;
    showWeights = 0;

    tracePixmap = QPixmap(this->size()); // Initialize with widget size
    tracePixmap.fill(Qt::transparent);  // Fill with transparent background
}

void mazeWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    drawMaze(&painter);

    drawRobot(2);
    if(showTrace){
        painter.drawPixmap(0, 0, tracePixmap);
        drawRobotMovementLine(3, QColor(0, 0, 255)); // Example: 3px blue line, color blue
    }
}

void mazeWidget::drawCell(int x, int y, QPainter *painter)
{
    QPen pens[4];
    getCellPens(x, y, pens);
    QLine lines[4];
    getCellLines(x, y, lines);

    for(int i=0; i<4; ++i){
        painter->setPen(pens[i]);
        painter->drawLine(lines[i]);
    }
}

void mazeWidget::drawMaze(QPainter *painter)
{

    for(int i=0; i<LAB_SIZE; ++i){
        for(int j=0; j<LAB_SIZE; ++j){
            drawCellBackground(j, i, painter);
            if(!labBoard->wasVisited(j, i)){
                drawCell(j, i, painter);
            }
        }
    }
    for(int i=0; i<LAB_SIZE; ++i){
        for(int j=0; j<LAB_SIZE; ++j){
            if(labBoard->wasVisited(j, i)){
                drawCell(j, i, painter);
            }
            if(showWeights){
                drawCellWeight(j, i, painter);
            }
        }
    }
}

void mazeWidget::drawRobot(float robotSizeDivider)
{
    int cellSideLength = ((this->width()-20)/LAB_SIZE);
    robotLabel.setFixedSize(cellSideLength/robotSizeDivider, cellSideLength/robotSizeDivider);
    QPixmap currentRobotIcon = robotIcon.scaled(cellSideLength/robotSizeDivider,
                                                cellSideLength/robotSizeDivider);

    char x, y;
    int angle;
    labBoard->getRobotPosioton(&x, &y, &angle);

    QTransform transform;
    transform.rotate(360-angle);
    currentRobotIcon = currentRobotIcon.transformed(transform, Qt::SmoothTransformation);

    robotLabel.setPixmap(currentRobotIcon);
    robotLabel.move(10+(x*cellSideLength + cellSideLength*((1-1/robotSizeDivider)/2)),
                    10+((LAB_SIZE-y-1)*cellSideLength + cellSideLength*(1-1/robotSizeDivider)/2));
}

void mazeWidget::drawRobotMovementLine(int lineWidthPixels, QColor lineColor) {
    if (!labBoard) return;

    char currX, currY, lastX, lastY;
    int angle;
    labBoard->getRobotPosioton(&currX, &currY, &angle);
    labBoard->getLastRobotPosioton(&lastX, &lastY, &angle);

    // If the positions haven't changed, no line needs to be drawn
    if (currX == lastX && currY == lastY) return;

    // Calculate the cell size
    int cellSideLength = (this->width() - 20) / LAB_SIZE;

    // Convert positions to pixel coordinates
    QPoint lastPos(10 + lastX * cellSideLength + cellSideLength / 2,
                   10 + (LAB_SIZE - lastY - 1) * cellSideLength + cellSideLength / 2);
    QPoint currPos(10 + currX * cellSideLength + cellSideLength / 2,
                   10 + (LAB_SIZE - currY - 1) * cellSideLength + cellSideLength / 2);

    // Create a QPainter for the trace pixmap
    QPainter tracePainter(&tracePixmap);
    QPen pen(lineColor);
    pen.setWidth(lineWidthPixels);
    tracePainter.setPen(pen);
    tracePainter.drawLine(lastPos, currPos);
}


void mazeWidget::getCellLines(int x, int y, QLine * lines)
{
    int cellSideLength = (this->width() - 20) / LAB_SIZE;

    // Define the offset to shorten the lines (e.g., 3 pixels)
    int offset = 5;

    // Calculate the original points for the cell corners
    QPoint leftBotCorner(10 + (x * cellSideLength), 10 + ((LAB_SIZE - y) * cellSideLength));
    QPoint leftTopCorner = leftBotCorner - QPoint(0, cellSideLength);
    QPoint rightTopCorner = leftTopCorner + QPoint(cellSideLength, 0);
    QPoint rightBotCorner = leftBotCorner + QPoint(cellSideLength, 0);

    // Adjust the line length but keep the center the same by moving the points towards the center
    QPoint horizontalOffset(offset, 0);
    QPoint verticalOffset(0, offset);

    //shorten lines only if they are not border lines
    // Top line
    if (y == LAB_SIZE - 1) {
        lines[0].setPoints(leftTopCorner, rightTopCorner);
    } else {
        lines[0].setPoints(leftTopCorner + horizontalOffset, rightTopCorner - horizontalOffset);
    }
    // Right line
    if (x == LAB_SIZE - 1) {
        lines[1].setPoints(rightTopCorner, rightBotCorner);
    } else {
        lines[1].setPoints(rightTopCorner + verticalOffset, rightBotCorner - verticalOffset);
    }
    // Bottom line
    if (y == 0) {
        lines[2].setPoints(rightBotCorner, leftBotCorner);
    } else {
        lines[2].setPoints(rightBotCorner - horizontalOffset, leftBotCorner + horizontalOffset);
    }
    // Left line
    if (x == 0) {
        lines[3].setPoints(leftBotCorner, leftTopCorner);
    } else {
        lines[3].setPoints(leftBotCorner - verticalOffset, leftTopCorner + verticalOffset);
    }
}


void mazeWidget::getCellPens(int x, int y, QPen * pens)
{
    if(labBoard->wasVisited(x, y)){
        QColor wallColor(255, 0, 0);
        QColor noWallColor(0, 255, 0);
        int powOfTwo = 1;
        for(int i=0; i<4; ++i){
            if((labBoard->getLabCell(x,y) & powOfTwo) == powOfTwo){
                pens[i].setColor(wallColor);
                pens[i].setWidth(4);
            }
            else{
                pens[i].setColor(noWallColor);
                pens[i].setWidth(2);
                pens[i].setStyle(Qt::DotLine);
            }
            powOfTwo *= 2;
        }
    }
    else{
        QColor innerWallColor(140, 140, 140);
        for(int i = 0; i<4; ++i){
            pens[i].setColor(innerWallColor);
            pens[i].setStyle(Qt::DotLine);
        }
    }

    //set outer border black and thicker
    QColor borderWallColor(0, 0, 0);
    if(y == LAB_SIZE-1){
        pens[0].setColor(borderWallColor);
        pens[0].setStyle(Qt::SolidLine);
        pens[0].setWidth(6);
    }
    if(x == LAB_SIZE-1){
        pens[1].setColor(borderWallColor);
        pens[1].setStyle(Qt::SolidLine);
        pens[1].setWidth(6);
    }
    if(y == 0){
        pens[2].setColor(borderWallColor);
        pens[2].setStyle(Qt::SolidLine);
        pens[2].setWidth(6);
    }
    if(x == 0){
        pens[3].setColor(borderWallColor);
        pens[3].setStyle(Qt::SolidLine);
        pens[3].setWidth(6);
    }
}

void mazeWidget::drawCellBackground(int x, int y, QPainter *painter)
{
    int cellSideLength = (this->width() - 20) / LAB_SIZE;

    // Calculate the top-left corner of the cell
    QPoint topLeft(10 + (x * cellSideLength), 10 + ((LAB_SIZE - y - 1) * cellSideLength));

    // Determine the color based on whether the cell was visited or if it is a goal
    QColor backgroundColor;
    if(labBoard->getFloodFillWeight(x, y) == 0){
        //green - a goal
        backgroundColor = QColor(0, 202, 27);
    }
    else if(labBoard->wasVisited(x, y)){
        //white - visited
        backgroundColor = QColor(255, 255, 255);
    }
    else {
        //grey - not visited
        backgroundColor = QColor(100, 100, 100);
    }

    // Set the brush for filling the cell background
    painter->setBrush(QBrush(backgroundColor));
    painter->setPen(Qt::NoPen); // No border for the background

    // Draw the rectangle for the cell background
    painter->drawRect(QRect(topLeft, QSize(cellSideLength, cellSideLength)));
}

void mazeWidget::drawCellWeight(int x, int y, QPainter *painter)
{
    int cellSideLength = (this->width() - 20) / LAB_SIZE;

    // Calculate the top-left corner of the cell
    QPoint topLeft(10 + (x * cellSideLength), 10 + ((LAB_SIZE - y - 1) * cellSideLength));

    // Get the number from the board
    int number = labBoard->getFloodFillWeight(x, y);

    // Set up font size based on cell size
    QFont font = painter->font();
    font.setPointSize(cellSideLength / 4); // Adjust font size to be relative to the cell size
    painter->setFont(font);

    // Set the text color
    painter->setPen(QColor(0, 0, 0)); // Black color for text

    // Calculate the position to center the text
    QRect cellRect(topLeft, QSize(cellSideLength, cellSideLength));
    painter->drawText(cellRect, Qt::AlignCenter, QString::number(number));
}

void mazeWidget::resizeEvent(QResizeEvent *event) {
    QPixmap newTracePixmap(event->size());
    newTracePixmap.fill(Qt::transparent);

    QPainter painter(&newTracePixmap);
    painter.drawPixmap(0, 0, tracePixmap);

    tracePixmap = newTracePixmap;

    QWidget::resizeEvent(event);
}
