#include "imuplot.h"
/*
imuPlot::imuPlot(QObject *parent)
    : QObject{parent}
{}*/

imuPlot::imuPlot(QList<QList<double> > *inputDataPointer)
{
    //mPlot(0);
    //mGraphX(0);
    //mGraphY(0);
    //mGraphZ(0);

    mPlot = new QCustomPlot();

    mPlot->yAxis->setTickLabels(true);
    connect(mPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), mPlot->yAxis, SLOT(setRange(QCPRange)));

    // create graphs:
    mGraphX = mPlot->addGraph(mPlot->xAxis, mPlot->axisRect()->axis(QCPAxis::atLeft, 0));
    mGraphX->setPen(QPen(QColor(255, 0, 0)));
    mGraphX->setName(tr("Oś X"));

    mGraphY = mPlot->addGraph(mPlot->xAxis, mPlot->axisRect()->axis(QCPAxis::atLeft, 0));
    mGraphY->setPen(QPen(QColor(0, 255, 0)));
    mGraphY->setName(tr("Oś Y"));

    mGraphZ = mPlot->addGraph(mPlot->xAxis, mPlot->axisRect()->axis(QCPAxis::atLeft, 0));
    mGraphZ->setPen(QPen(QColor(0, 0, 255)));
    mGraphZ->setName(tr("Oś Z"));



    mPlot->legend->setVisible(true);
    /*QFont legendFont = font();
    legendFont.setPointSize(9);
    mPlot->legend->setFont(legendFont);*/
    mPlot->legend->setBrush(QBrush(QColor(255,255,255,230)));
    mPlot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignLeft|Qt::AlignTop);
    mPlot->xAxis->setLabel(tr("Czas[s]"));

    dataPointer = inputDataPointer;

    connect(&mDataTimer, SIGNAL(timeout()), this, SLOT(timerSlot()));
    mDataTimer.start(100);


}

void imuPlot::changePlotData(QList<QList<double> > *inputDataPointer)
{
    this->dataPointer = inputDataPointer;
    mGraphX->data()->clear();
    mGraphY->data()->clear();
    mGraphZ->data()->clear();
}

void imuPlot::retranslateSeriesNames()
{
    mGraphX->setName(tr("Oś X"));
    mGraphY->setName(tr("Oś Y"));
    mGraphZ->setName(tr("Oś Z"));
    mPlot->xAxis->setLabel(tr("Czas[s]"));

}


void imuPlot::timerSlot()
{
    // calculate and add a new data point to each graph:

    //mGraphX->addData(mGraphX->dataCount(), qSin(mGraphX->dataCount()/50.0)+qSin(mGraphX->dataCount()/50.0/0.3843)*0.25);
    for(int i = 0; i < (*dataPointer)[0].size(); ++i)
    {
        //qDebug() << accData[0].front() << " ";
        mGraphX->addData(mGraphX->dataCount()/5, (*dataPointer)[0].front());
        (*dataPointer)[0].pop_front();

        mGraphY->addData(mGraphY->dataCount()/5, (*dataPointer)[1].front());
        (*dataPointer)[1].pop_front();

        mGraphZ->addData(mGraphZ->dataCount()/5, (*dataPointer)[2].front());
        (*dataPointer)[2].pop_front();
    }
    //qDebug() << "\n";
    // make key axis range scroll with the data:
    mPlot->xAxis->rescale();
    //mGraphX->rescaleValueAxis(false, true);
    //mGraphY->rescaleValueAxis(true, true);
    //mGraphZ->rescaleValueAxis(true, true);
    mPlot->yAxis->rescale(false);
    mPlot->xAxis->setRange(mPlot->xAxis->range().upper, 20, Qt::AlignRight);
    mPlot->replot();
}
