#ifndef MAZEWIDGET_H
#define MAZEWIDGET_H

#include <QObject>
#include <QWidget>
#include <QPainter>
#include <QLine>
#include <QColor>
#include <QPixmap>
#include <QLabel>
#include <QResizeEvent>

#include "board.h"

#define ROBOT_IMAGE_PATH "C:/Users/filip/Desktop/Studia/Micromouse/micromouse-qt-visualization/robot_transparent.png"

/**
 * @class mazeWidget
 * @brief The mazeWidget class represents a widget for displaying a maze.
 *
 * The mazeWidget class represents a widget for displaying a maze.
 */
class mazeWidget : public QWidget
{
    Q_OBJECT
public:
    bool showTrace;
    bool showWeights;
    /**
     * @brief Constructs a mazeWidget object.
     *
     * Constructs a mazeWidget object.
     * @param[in] parent - The parent widget (default is nullptr).
     */
    explicit mazeWidget(QWidget *parent = nullptr);

    /**
     * @brief Constructs a mazeWidget object with a specified board.
     *
     * Constructs a mazeWidget object with a specified board.
     * @param[in] boardToAdd - Pointer to the board object to use.
     */
    explicit mazeWidget(board *boardToAdd);

    /**
     * @brief Handles the paint event to draw the maze.
     *
     * Handles the paint event to draw the maze.
     * @param[in] event - The paint event.
     */
    virtual void paintEvent(QPaintEvent *event);

private:
    /// Pointer to the board object representing the labyrinth.
    board *labBoard;

    /// Pixmap representing the robot icon.
    QPixmap robotIcon;

    /// Label for the robot.
    QLabel robotLabel;

    // To store the robot's movement trail
    QPixmap tracePixmap;

    /**
     * @brief Draws a single cell of the maze at the specified coordinates.
     *
     * Draws a single cell of the maze at the specified coordinates.
     * @param[in] x - The X-coordinate of the cell.
     * @param[in] y - The Y-coordinate of the cell.
     * @param[in] painter - Pointer to the QPainter object used for drawing.
     */
    void drawCell(int x, int y, QPainter *painter);

    /**
     * @brief Draws the entire maze.
     *
     * Draws the entire maze.
     * @param[in] painter - Pointer to the QPainter object used for drawing.
     */
    void drawMaze(QPainter *painter);

    /**
     * @brief Draws the robot on the maze.
     *
     * Draws the robot on the maze.
     * @param[in] robotSizeDivider - The size divider to scale the robot.
     */
    void drawRobot(float robotSizeDivider);

    void drawRobotMovementLine(int lineWidthPixels, QColor lineColor);

    /**
     * @brief Gets the lines representing the walls of a cell in the order: top, right, bottom, left.
     *
     * Gets the lines representing the walls of a cell in the order: top, right, bottom, left.
     * @param[in] x - The X-coordinate of the cell.
     * @param[in] y - The Y-coordinate of the cell.
     * @param[out] lines - Pointer to the array of QLine objects to store the wall lines.
     */
    void getCellLines(int x, int y, QLine *lines);

    /**
     * @brief Gets the pens used for drawing the walls of a cell in the order: top, right, bottom, left.
     *
     * Gets the pens used for drawing the walls of a cell in the order: top, right, bottom, left.
     * @param[in] x - The X-coordinate of the cell.
     * @param[in] y - The Y-coordinate of the cell.
     * @param[out] pens - Pointer to the array of QPen objects to store the pens.
     */
    void getCellPens(int x, int y, QPen *pens);

    void drawCellBackground(int x, int y, QPainter *painter);

    void drawCellWeight(int x, int y, QPainter *painter);

    virtual void resizeEvent(QResizeEvent *event) override;
signals:
};

#endif // MAZEWIDGET_H
