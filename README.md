# Micromouse Qt visualization 

## About project
This project was created for academic purpouses. It's main target is to visualize process of labirynth solving of Micromouse robot. Project is develiped in Qt 6.7.0 using widgets. Additionally, in STM32_test_project, you can find programm generating example test data for debuggin and development purposuses.

## Communication with robot
At this point, communication is based on serial port, in the future this will be replaced with Wi-Fi communication.

## Running project
Simplest way to run and explore project is to open project folder with Qt Creator, run and compile it with build-in tools.
To see all features of project, connecting STM32l476RG microcontroller with provided test programm is recommended.
Note that changing serial port name might be necessary. You can do that in "serialdataaqusition.cpp" file in initSerialPort() function. 