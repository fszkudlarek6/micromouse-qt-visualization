#ifndef SERIALDATAAQUISITION_H
#define SERIALDATAAQUISITION_H

#include <sstream>
#include <QObject>
#include <QDebug>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QString>
#include "board.h"

#define SERIAL_PORT_NAME "COM7"

/**
 * @class serialDataAquisition
 * @brief The serialDataAquisition class handles data acquisition from a serial port.
 *
 * The serialDataAquisition class handles data acquisition from a serial port.
 */
class serialDataAquisition : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Constructs a serialDataAquisition object.
     *
     * Constructs a serialDataAquisition object.
     * @param[in] board - Pointer to the board object.
     */
    explicit serialDataAquisition(board *board);

    /**
     * @brief Destructor for the serialDataAquisition object.
     *
     * Destructor for the serialDataAquisition object.
     */
    ~serialDataAquisition();

    /// List of lists to store accelerometer data.
    QList<QList<double>> accData;

    /// List of lists to store gyroscope data.
    QList<QList<double>> gyrData;

    /// List of lists to store magnetometer data.
    QList<QList<double>> magData;

private:
    /// Pointer to the QSerialPort object.
    QSerialPort *serialPort;

    /**
     * @brief Initializes the serial port.
     *
     * Initializes the serial port.
     */
    void initSerialPort();

    /// Pointer to the board object.
    board *labBoard;

    /// String to store data received from the serial port.
    QString dataFromSerial;

    /// Flag indicating whether data has been received.
    bool isDataReceived = false;

    /**
     * @brief Converts serial data to a vector.
     *
     * Converts serial data to a vector.
     * @param[out] dataVect - Reference to the data vector to fill.
     * @param[in] dataSS - Reference to the stringstream containing the data.
     * @param[in] mode - The mode indicating the type of data.
     */
    void dataToVect(QList<QList<double>> &dataVect, std::stringstream &dataSS, int mode);

    /**
     * @brief Updates the board with data from the stringstream.
     *
     * Updates the board with data from the stringstream.
     * @param[in] dataSS - Reference to the stringstream containing the data.
     */
    void dataToBoard(std::stringstream &dataSS);

    void dataToFloodFill(std::stringstream &dataSS);

    /**
     * @brief Updates the robot with data from the stringstream.
     *
     * Updates the robot with data from the stringstream.
     * @param[in] dataSS - Reference to the stringstream containing the data.
     */
    void dataToRobot(std::stringstream &dataSS);

    /**
     * @brief Calculates the CRC for the given data string.
     *
     * Calculates the CRC for the given data string.
     * @param[in] dataS - Reference to the data string.
     * @return The calculated CRC value.
     */
    uint32_t calc_crc(std::string &dataS);

private slots:
    /**
     * @brief Slot function for reading data from the serial port.
     *
     * Slot function for reading data from the serial port.
     */
    void readData();
};

#endif // SERIALDATAAQUISITION_H
