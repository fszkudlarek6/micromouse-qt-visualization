#ifndef BOARD_H
#define BOARD_H

#include <QObject>
#include <QDebug>
#include <string>

#define LAB_SIZE 16
//magic number, to ensure, that each weight takes 2 digits to write
//CAUTIOUS -- it is added in the MC before sending and should be substracted in the app
#define FLOODFILL_SENDING_ADDITION 10

/**
 * @class board
 * @brief The board class represents a labyrinth board and the robot's state within it.
 *
 * The board class represents a labyrinth board and the robot's state within it.
 */
class board : public QObject
{
    Q_OBJECT

    /// The 2D array representing the labyrinth.
    char boardTab[LAB_SIZE][LAB_SIZE];

    /// The angle of the robot's position in degrees. Zero corresponds to robot facing right.
    int robotPositionAngleDegrees;
    int lastRobotPositionAngleDegrees;

    /// The X-coordinate of the robot's position. Unit is width/height of one cell.
    char robotPosionX;
    char lastRobotPosionX;

    /// The Y-coordinate of the robot's position. Unit is width/height of one cell.
    char robotPosionY;
    char lastRobotPosionY;

    int floodFillWeightsTab[LAB_SIZE][LAB_SIZE];

public:
    /**
     * @brief Constructs a board object.
     *
     * Constructs a board object.
     * @param[in] parent - The parent QObject (default is nullptr).
     */
    explicit board(QObject *parent = nullptr);

    /**
     * @brief Updates the labyrinth with new data.
     *
     * Updates the labyrinth with new data.
     * @param[in] codedLabData - The new labyrinth data encoded as a string.
     */
    void updateLabirinth(std::string codedLabData);

    void updateFloodFill(std::string codedFloodFillData);

    /**
     * @brief Updates the robot's position and angle.
     *
     * Updates the robot's position and angle.
     * @param[in] x - The new X-coordinate of the robot.
     * @param[in] y - The new Y-coordinate of the robot.
     * @param[in] angDeg - The new angle of the robot in degrees.
     */
    void updateRobot(int x, int y, int angDeg);

    /**
     * @brief Outputs the current state of the labyrinth to QDebug.
     *
     * Outputs the current state of the labyrinth to QDebug.
     */
    void mazeToQDebug();

    /**
     * @brief Gets the value of a specific cell in the labyrinth.
     *
     * Gets the value of a specific cell in the labyrinth.
     * @param[in] x - The X-coordinate of the cell.
     * @param[in] y - The Y-coordinate of the cell.
     * @return The value of the cell at (x, y).
     */
    char getLabCell(int x, int y);

    /**
     * @brief Gets the current position and angle of the robot.
     *
     * Gets the current position and angle of the robot.
     * @param[out] x - Pointer to store the X-coordinate of the robot.
     * @param[out] y - Pointer to store the Y-coordinate of the robot.
     * @param[out] angDeg - Pointer to store the angle of the robot (degrees).
     */
    void getRobotPosioton(char *x, char *y, int *angDeg);

    void getLastRobotPosioton(char *x, char *y, int *angDeg);

    /**
     * @brief Adds a top wall to the cell at (x, y).
     *
     * Adds a top wall to the cell at (x, y).
     * @param[in] x - The X-coordinate of the cell.
     * @param[in] y - The Y-coordinate of the cell.
     */
    void addTopWall(int x, int y);

    /**
     * @brief Adds a right wall to the cell at (x, y).
     *
     * Adds a right wall to the cell at (x, y).
     * @param[in] x - The X-coordinate of the cell.
     * @param[in] y - The Y-coordinate of the cell.
     */
    void addRightWall(int x, int y);

    /**
     * @brief Adds a bottom wall to the cell at (x, y).
     *
     * Adds a bottom wall to the cell at (x, y).
     * @param[in] x - The X-coordinate of the cell.
     * @param[in] y - The Y-coordinate of the cell.
     */
    void addBotWall(int x, int y);

    /**
     * @brief Adds a left wall to the cell at (x, y).
     *
     * Adds a left wall to the cell at (x, y).
     * @param[in] x - The X-coordinate of the cell.
     * @param[in] y - The Y-coordinate of the cell.
     */
    void addLeftWall(int x, int y);

    /**
     * @brief Marks the cell at (x, y) as visited.
     *
     * Marks the cell at (x, y) as visited.
     * @param[in] x - The X-coordinate of the cell.
     * @param[in] y - The Y-coordinate of the cell.
     */
    void markVisited(int x, int y);

    /**
     * @brief Checks if there is a top wall at the cell (x, y).
     *
     * Checks if there is a top wall at the cell (x, y).
     * @param[in] x - The X-coordinate of the cell.
     * @param[in] y - The Y-coordinate of the cell.
     * @retval True - there is a top wall.
     * @retval False - there is no top wall.
     */
    bool hasTopWall(int x, int y);

    /**
     * @brief Checks if there is a right wall at the cell (x, y).
     *
     * Checks if there is a right wall at the cell (x, y).
     * @param[in] x - The X-coordinate of the cell.
     * @param[in] y - The Y-coordinate of the cell.
     * @retval True - there is a right wall.
     * @retval False - there is no right wall.
     */
    bool hasRightWall(int x, int y);

    /**
     * @brief Checks if there is a bottom wall at the cell (x, y).
     *
     * Checks if there is a bottom wall at the cell (x, y).
     * @param[in] x - The X-coordinate of the cell.
     * @param[in] y - The Y-coordinate of the cell.
     * @retval True - there is a bottom wall.
     * @retval False - there is no bottom wall.
     */
    bool hasBotWall(int x, int y);

    /**
     * @brief Checks if there is a left wall at the cell (x, y).
     *
     * Checks if there is a left wall at the cell (x, y).
     * @param[in] x - The X-coordinate of the cell.
     * @param[in] y - The Y-coordinate of the cell.
     * @retval True - there is a left wall.
     * @retval False - there is no left wall.
     */
    bool hasLeftWall(int x, int y);

    /**
     * @brief Checks if the cell at (x, y) was visited.
     *
     * Checks if the cell at (x, y) was visited.
     * @param[in] x - The X-coordinate of the cell.
     * @param[in] y - The Y-coordinate of the cell.
     * @retval True - the cell was visited.
     * @retval False - the cell was not visited.
     */
    bool wasVisited(int x, int y);

    void setFloodFillWeight(int x, int y, int weight);

    int getFloodFillWeight(int x, int y);

    int getLowestNeighbiourWeight(int x, int y);

    void calculateManhattanDistanceForCell(int x, int y);

    void floodCell(int x, int y);

    void calculateManhattanDistanceForNeighbours(int x, int y);

    void floodNeighbours(int x, int y);
    //calculates the Manhattan distance for each cell and writes it
    void initialFloodManhattanDistance();

signals:
};

#endif // BOARD_H
