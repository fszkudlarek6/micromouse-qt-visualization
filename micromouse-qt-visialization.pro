QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

QT += serialport

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    board.cpp \
    imuplot.cpp \
    main.cpp \
    mainwindow.cpp \
    mazewidget.cpp \
    qcustomplot.cpp \
    serialdataaquisition.cpp

HEADERS += \
    board.h \
    imuplot.h \
    mainwindow.h \
    mazewidget.h \
    qcustomplot.h \
    serialdataaquisition.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

TRANSLATIONS += \
    micromouse_qt_visialization_en_US.ts

DISTFILES += \
    micromouse_qt_visialization_en_US.qm
