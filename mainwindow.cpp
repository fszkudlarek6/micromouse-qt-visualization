#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    ui(new Ui::MainWindow)

{
    ui->setupUi(this);
    this->setWindowTitle(tr("Wizualizacja działania robota MicroMouse"));

    if(mTraslatorEn.load("C:/Users/filip/Desktop/Studia/Micromouse/micromouse-qt-visualization/"
                          "micromouse_qt_visialization_en_US.qm")){
        qDebug() << "Translation loaded succesfully";
    }
    else{
        qDebug() << "Translation NOT LOADED!";
    }

    serialObject = new serialDataAquisition(&labBoard);

    initImuPlot();

    initMazeWidget();
    connect(ui->comboBoxSens, SIGNAL(currentTextChanged(QString)),
            this, SLOT(sensorComboBoxSlot()));
    connect(ui->comboBoxLang, SIGNAL(currentTextChanged(QString)),
            this, SLOT(languageComboBoxSlot()));

    connect(ui->comboBoxTrace, SIGNAL(currentTextChanged(QString)),
            this, SLOT(traceComboBoxSlot()));
    connect(ui->comboBoxWeights, SIGNAL(currentTextChanged(QString)),
            this, SLOT(weightsComboBoxSlot()));

    connect(&debugTimer, SIGNAL(timeout()), this, SLOT(timerSlot()));
    debugTimer.start(500);
    ui->comboBoxSens->hide();
    ui->comboBoxLang->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::timerSlot()
{
    mazeObject->update();
}

void MainWindow::sensorComboBoxSlot()
{
    QString comboState =  ui->comboBoxSens->currentText();
    if(comboState == tr("Akcelerometr")){
        plotObjectAcc->mPlot->setVisible(false);
        plotObjectGyro->mPlot->setVisible(false);
        plotObjectMag->mPlot->setVisible(false);
    }
    else if(comboState == tr("Magnetometr")){
        plotObjectAcc->mPlot->setVisible(false);
        plotObjectGyro->mPlot->setVisible(false);
        plotObjectMag->mPlot->setVisible(false);
    }
    else if(comboState == tr("Żyroskop")){
        plotObjectAcc->mPlot->setVisible(false);
        plotObjectGyro->mPlot->setVisible(false);
        plotObjectMag->mPlot->setVisible(false);
    }
}

void MainWindow::languageComboBoxSlot()
{
    QString comboState =  ui->comboBoxLang->currentText();
    if(comboState == "En"){
        qApp->installTranslator(&mTraslatorEn);
    }
    else if(comboState == "Pl"){
        qApp->removeTranslator(&mTraslatorEn);
    }
}

void MainWindow::traceComboBoxSlot()
{
    QString comboState =  ui->comboBoxTrace->currentText();
    qDebug() << "Trace Combo Changed: " << comboState;
    if(comboState == "Wyświetl ścieżkę"){
        mazeObject->showTrace = true;
    }
    else if(comboState == "Ukryj ścieżkę"){
        mazeObject->showTrace = false;
    }
    mazeObject->update();
}

void MainWindow::weightsComboBoxSlot()
{
    QString comboState =  ui->comboBoxWeights->currentText();
    qDebug() << "Weights Combo Changed: " << comboState;
    if(comboState == tr("Wyświetl wagi")){
        mazeObject->showWeights = true;
    }
    else if(comboState == tr("Ukryj wagi")){
        mazeObject->showWeights = false;
    }
    mazeObject->update();
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    QMainWindow::resizeEvent(event);
    resizeMazeWidget();
}

void MainWindow::resizeMazeWidget()
{
    if (!mazeObject)
        return;

    // Get the current size of the central widget or main window
    QSize currentSize = this->size();
    int width = currentSize.width();
    int height = currentSize.height()*0.9;

    // Maintain aspect ratio
    int size = qMin(width, height);

    // Set the size of the mazeWidget to be a square
    mazeObject->setFixedSize(size, size);
}

void MainWindow::initImuPlot()
{
    QFont legendFont = font();
    legendFont.setPointSize(9);

    plotObjectAcc = new imuPlot(&(serialObject->accData));
    ui->layoutChartRightUp->addWidget(plotObjectAcc->mPlot);
    plotObjectAcc->mPlot->legend->setVisible(true);
    plotObjectAcc->mPlot->legend->setFont(legendFont);
    plotObjectAcc->mPlot->yAxis->setLabel(tr("Przyspieszenie[g]"));
    plotObjectAcc->mPlot->setVisible(false);


    plotObjectGyro = new imuPlot(&(serialObject->gyrData));
    ui->layoutChartRightMid->addWidget(plotObjectGyro->mPlot);
    plotObjectGyro->mPlot->legend->setVisible(true);
    plotObjectGyro->mPlot->legend->setFont(legendFont);
    plotObjectGyro->mPlot->yAxis->setLabel(tr("Prędkość kątowa[stopnie/s]"));
    plotObjectGyro->mPlot->setVisible(false);


    plotObjectMag = new imuPlot(&(serialObject->magData));
    ui->layoutChartRightDown->addWidget(plotObjectMag->mPlot);
    plotObjectMag->mPlot->legend->setVisible(true);
    plotObjectMag->mPlot->legend->setFont(legendFont);
    plotObjectMag->mPlot->yAxis->setLabel(tr("Natężenie pola[Gauss]"));
    plotObjectMag->mPlot->setVisible(false);
}

void MainWindow::retranslateImuPlot()
{
    plotObjectAcc->retranslateSeriesNames();
    plotObjectMag->retranslateSeriesNames();
    plotObjectGyro->retranslateSeriesNames();
    plotObjectAcc->mPlot->yAxis->setLabel(tr("Przyspieszenie[g]"));
    plotObjectGyro->mPlot->yAxis->setLabel(tr("Prędkość kątowa[stopnie/s]"));
    plotObjectMag->mPlot->yAxis->setLabel(tr("Natężenie pola[Gauss]"));
}

void MainWindow::initMazeWidget()
{
    mazeObject = new mazeWidget(&labBoard);
    ui->layoutLabLeft->addWidget(mazeObject);
    mazeObject->setMinimumSize(100, 100);
}

void MainWindow::changeEvent(QEvent * event)
{
    if(event->type() == QEvent::LanguageChange){
        ui->retranslateUi(this);
        retranslateImuPlot();
    }

    this->setWindowTitle(tr("Wizualizacja działania robota MicroMouse"));
    QWidget::changeEvent(event);
}
