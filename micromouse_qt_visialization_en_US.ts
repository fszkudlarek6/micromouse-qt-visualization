<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="34"/>
        <source>Wizualizacja labiryntu</source>
        <translation>Maze visualization</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="63"/>
        <source>Akcelerometr</source>
        <translation>Accelerometer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="68"/>
        <source>Magnetometr</source>
        <translation>Magnetometer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="73"/>
        <source>Żyroskop</source>
        <translation>Gyroscope</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="10"/>
        <source>Wizualizacja działania robota MicroMouse</source>
        <translation>Visualization of functioning of MicroMouse robot</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="86"/>
        <source>Przyspieszenie[g]</source>
        <translation>Acceleration[g]</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="94"/>
        <source>Prędkość kątowa[stopnie/s]</source>
        <translation>Angular velocity[deg/sec]</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="102"/>
        <source>Natężenie pola[Gauss]</source>
        <translation>Field strength[Gauss]</translation>
    </message>
</context>
<context>
    <name>imuPlot</name>
    <message>
        <location filename="imuplot.cpp" line="22"/>
        <source>Oś X</source>
        <translation>X Axis</translation>
    </message>
    <message>
        <location filename="imuplot.cpp" line="26"/>
        <source>Oś Y</source>
        <translation>Y Axis</translation>
    </message>
    <message>
        <location filename="imuplot.cpp" line="30"/>
        <source>Oś Z</source>
        <translation>Z Axis</translation>
    </message>
    <message>
        <location filename="imuplot.cpp" line="40"/>
        <source>Czas[s]</source>
        <translation>Time[s]</translation>
    </message>
</context>
</TS>
