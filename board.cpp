#include "board.h"

board::board(QObject *parent)
    : QObject{parent}
{
    for(int i = 0; i<LAB_SIZE; ++i){
        for(int j = 0; j<LAB_SIZE; ++j){
            //each cell has no walls and wasn't visited
            boardTab[j][i] = 0;
            //setting to some high value to perform actual flood fill
            floodFillWeightsTab[j][i] = 600;
        }
    }
    robotPosionX = 0;
    robotPosionY = 0;

    //initialFloodManhattanDistance();
}

void board::updateLabirinth(std::string codedLabData)
{
    char labData[LAB_SIZE*LAB_SIZE];
    for(size_t i = 0; i<LAB_SIZE*LAB_SIZE*2; i=i+2){
        std::string tempStr = codedLabData.substr(i, 2);
        labData[i/2] = std::stoi(tempStr);
    }

    for(int i = 0; i<LAB_SIZE; ++i){
        for(int j=0; j<LAB_SIZE; ++j){
            boardTab[j][i] = labData[j + LAB_SIZE*i];
        }
    }
}

void board::updateFloodFill(std::string codedFloodFillData)
{
    char floodFillData[LAB_SIZE*LAB_SIZE];
    for(size_t i = 0; i<LAB_SIZE*LAB_SIZE*2; i=i+2){
        std::string tempStr = codedFloodFillData.substr(i, 2);
        floodFillData[i/2] = std::stoi(tempStr);
    }

    for(int i = 0; i<LAB_SIZE; ++i){
        for(int j=0; j<LAB_SIZE; ++j){
            floodFillWeightsTab[j][i] = floodFillData[j + LAB_SIZE*i]
                - FLOODFILL_SENDING_ADDITION;
        }
    }
}

void board::updateRobot(int x, int y, int ang){
    lastRobotPosionX = robotPosionX;
    lastRobotPosionY = robotPosionY;
    lastRobotPositionAngleDegrees = robotPositionAngleDegrees;

    robotPosionX = (char)x;
    robotPosionY = (char)y;
    robotPositionAngleDegrees = ang;
    //flood the cell, that robot is currently in
    //floodCell(x, y);

    /*qDebug()<<"Robot";
    qDebug()<< std::to_string(robotPosionX);
    qDebug()<< std::to_string(robotPosionY);
    qDebug()<<robotPositionAngle;*/
}

void board::mazeToQDebug()
{
    qDebug() << "Labir";
    std::string oneLine;
    for(int i=LAB_SIZE -1; i>=0; --i){
        for(int j=0; j<LAB_SIZE; ++j){
            oneLine += std::to_string(getLabCell(j, i)) + " ";
        }
        qDebug() << oneLine;
        oneLine = "";
    }
}

char board::getLabCell(int x, int y)
{
    return this->boardTab[x][y];
}

void board::getRobotPosioton(char *x, char *y, int *ang)
{
    *x = robotPosionX;
    *y = robotPosionY;
    *ang = robotPositionAngleDegrees;
}

void board::getLastRobotPosioton(char *x, char *y, int *ang)
{
    *x = lastRobotPosionX;
    *y = lastRobotPosionY;
    *ang = lastRobotPositionAngleDegrees;
}

void board::addTopWall(int x, int y){
    this->boardTab[x][y] = (this->boardTab[x][y]|1);
}
//second least significant bit is right wall
void board::addRightWall(int x, int y){
    this->boardTab[x][y] = (this->boardTab[x][y]|2);
}
//third least significant bit is bottom wall
void board::addBotWall(int x, int y){
    this->boardTab[x][y] = (this->boardTab[x][y]|4);
}
//fourth least significant bit is left wall
void board::addLeftWall(int x, int y){
    this->boardTab[x][y] = (this->boardTab[x][y]|8);
}
//fifth least significant bit says if cell was visited 0-wasn't, 1-was
void board::markVisited(int x, int y){
    this->boardTab[x][y] = (this->boardTab[x][y]|16);
}

bool board::hasTopWall(int x, int y){
    return((this->boardTab[x][y]&1) == 1);
}
//second least significant bit is right wall
bool board::hasRightWall(int x, int y){
    return((this->boardTab[x][y]&2) == 2);
}
//third least significant bit is bottom wall
bool board::hasBotWall(int x, int y){
    return((this->boardTab[x][y]&4) == 4);
}
//fourth least significant bit is left wall
bool board::hasLeftWall(int x, int y){
    return ((this->boardTab[x][y]&8) == 8);
}
//fifth least significant bit says if cell was visited 0-wasn't, 1-was
bool board::wasVisited(int x, int y){
    return ((this->boardTab[x][y]&16) == 16);
}

void board::setFloodFillWeight(int x, int y, int weight){
    this->floodFillWeightsTab[x][y] = weight;
}

int board::getFloodFillWeight(int x, int y){
    return this->floodFillWeightsTab[x][y];
}

int board::getLowestNeighbiourWeight(int x, int y){
    //first posiotion -top; second - right; third - bot, fourth - left
    int weights[4] = {600, 600, 600, 600};
    //check, which cells can be entered if cell was visited
    if(wasVisited(x, y)){
        if(!hasTopWall(x, y)){
            weights[0] = getFloodFillWeight(x, y+1);
        }
        if(!hasRightWall(x, y)){
            weights[1] = getFloodFillWeight(x+1, y);
        }
        if(!hasBotWall(x, y)){
            weights[2] = getFloodFillWeight(x, y-1);
        }
        if(!hasLeftWall(x, y)){
            weights[3] = getFloodFillWeight(x-1, y);
        }
    }
    //assume no walls if cell wasn't visited
    else {
        if(y < (LAB_SIZE-1)){
            weights[0] = getFloodFillWeight(x, y+1);
        }
        if(x < (LAB_SIZE-1)){
            weights[1] = getFloodFillWeight(x+1, y);
        }
        if(y > 0){
            weights[2] = getFloodFillWeight(x, y-1);
        }
        if(x > 0){
            weights[3] = getFloodFillWeight(x-1, y);
        }
    }
    int lowest_weight = 600;
    for(uint8_t i =0; i<4; ++i){
        if(weights[i] < lowest_weight){
            lowest_weight = weights[i];
        }
    }
    return lowest_weight;
}

void board::calculateManhattanDistanceForCell(int x, int y){
    int lowestNeighbourWeight = getLowestNeighbiourWeight(x, y);
    if(wasVisited(x, y)){
        if((lowestNeighbourWeight+1) < getFloodFillWeight(x, y)){
            setFloodFillWeight(x, y, lowestNeighbourWeight+1);
            calculateManhattanDistanceForNeighbours(x, y);
        }
    }
    else {
        if((lowestNeighbourWeight+1) < getFloodFillWeight(x, y)){
            setFloodFillWeight(x, y, lowestNeighbourWeight+1);
            calculateManhattanDistanceForNeighbours(x, y);
        }
    }
}

void board::floodCell(int x, int y){
    int lowestNeighbourWeight = getLowestNeighbiourWeight(x, y);
    if(getFloodFillWeight(x, y) <= (lowestNeighbourWeight)){
        setFloodFillWeight(x, y, lowestNeighbourWeight+1);
        floodNeighbours(x, y);
    }
}

void board::calculateManhattanDistanceForNeighbours(int x, int y){
    //flood adjacent cells, that can be entered from first cell
    if(wasVisited(x, y)){
        if(!hasTopWall(x, y)){
            calculateManhattanDistanceForCell(x, y+1);
        }
        if(!hasRightWall(x, y)){
            calculateManhattanDistanceForCell(x+1, y);
        }
        if(!hasBotWall(x, y)){
            calculateManhattanDistanceForCell(x, y-1);
        }
        if(!hasLeftWall(x, y)){
            calculateManhattanDistanceForCell(x-1, y);
        }
    }
    //assume no walls if cell wasn't visited
    else {
        if(y < (LAB_SIZE-1)){
            calculateManhattanDistanceForCell(x, y+1);
        }
        if(x < (LAB_SIZE-1)){
            calculateManhattanDistanceForCell(x+1, y);
        }
        if(y > 0){
            calculateManhattanDistanceForCell(x, y-1);
        }
        if(x > 0){
            calculateManhattanDistanceForCell(x-1, y);
        }
    }
}

void board::floodNeighbours(int x, int y){
    //flood adjacent cells, that can be entered from first cell
    if(wasVisited(x, y)){
        if(!hasTopWall(x, y)){
            floodCell(x, y+1);
        }
        if(!hasRightWall(x, y)){
            floodCell(x+1, y);
        }
        if(!hasBotWall(x, y)){
            floodCell(x, y-1);
        }
        if(!hasLeftWall(x, y)){
            floodCell(x-1, y);
        }
    }
    //assume no walls if cell wasn't visited
    else {
        if(y < (LAB_SIZE-1)){
            floodCell(x, y+1);
        }
        if(x < (LAB_SIZE-1)){
            floodCell(x+1, y);
        }
        if(y > 0){
            floodCell(x, y-1);
        }
        if(x > 0){
            floodCell(x-1, y);
        }
    }
}

void board::initialFloodManhattanDistance(){
    int higher_goal_dim = LAB_SIZE/2;
    int lower_goal_dim = higher_goal_dim - 1;
    //set goal cells weights to 0
    setFloodFillWeight(lower_goal_dim, lower_goal_dim, 0);
    setFloodFillWeight(lower_goal_dim, higher_goal_dim, 0);
    setFloodFillWeight(higher_goal_dim, higher_goal_dim, 0);
    setFloodFillWeight(higher_goal_dim, lower_goal_dim, 0);
    //flood goal cells
    calculateManhattanDistanceForCell(6, 7);
}
