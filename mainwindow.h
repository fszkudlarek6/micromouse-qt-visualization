#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPainter>
#include <QTranslator>
#include "serialdataaquisition.h"
#include "imuplot.h"
#include "board.h"
#include "mazewidget.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

/**
 * @class MainWindow
 * @brief The MainWindow class represents the main application window.
 *
 * The MainWindow class represents the main application window.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief Constructs a MainWindow object.
     *
     * Constructs a MainWindow object.
     * @param parent The parent widget (default is nullptr).
     */
    MainWindow(QWidget *parent = nullptr);

    /**
     * @brief Destructor for the MainWindow object.
     *
     * Destructor for the MainWindow object.
     */
    ~MainWindow();

private slots:
    /**
     * @brief Slot function for handling timer events.
     *
     * Slot function for handling timer events. Updates maze.
     */
    void timerSlot();

    /**
     * @brief Slot function for handling sensor comboBox events.
     *
     * Slot function for handling sensor comboBox events. Sets visible currently set sensor chart.
     */
    void sensorComboBoxSlot();

    /**
     * @brief Slot function for handling language comboBox events.
     *
     * Slot function for handling language comboBox events. Trasn.
     */
    void languageComboBoxSlot();

    void traceComboBoxSlot();
    void weightsComboBoxSlot();


private:
    /// Pointer to the UI object.
    Ui::MainWindow *ui;

    /// Pointer to the serial data acquisition object.
    serialDataAquisition * serialObject;

    /// Pointer to the IMU plot object for accelerometer data.
    imuPlot * plotObjectAcc;

    /// Pointer to the IMU plot object for gyroscope data.
    imuPlot * plotObjectGyro;

    /// Pointer to the IMU plot object for magnetometer data.
    imuPlot * plotObjectMag;

    /// Pointer to the maze widget object.
    mazeWidget * mazeObject;

    /// Timer for debugging purposes.
    QTimer debugTimer;

    /// Board object representing the labyrinth.
    board labBoard;

    /// Translator object containg traslations to english (US).
    QTranslator mTraslatorEn;

    /**
     * @brief Handles the resize event to adjust the layout.
     *
     * Handles the resize event to adjust the layout.
     * @param[in] event The resize event.
     */
    void resizeEvent(QResizeEvent *event) override;

    /**
     * @brief Resizes the maze widget.
     *
     * Resizes the maze widget.
     */
    void resizeMazeWidget();

    /**
     * @brief Initializes the IMU plot objects.
     *
     * Initializes the IMU plot objects.
     */
    void initImuPlot();

    /**
     * @brief Retranslates all ImuPlot objects.
     *
     * Retranslates all ImuPlot objects.
     */
    void retranslateImuPlot();

    /**
     * @brief Initializes the maze widget.
     *
     * Initializes the maze widget.
     */
    void initMazeWidget();

    // QWidget interface
protected:
    void changeEvent(QEvent *);
};

#endif // MAINWINDOW_H
